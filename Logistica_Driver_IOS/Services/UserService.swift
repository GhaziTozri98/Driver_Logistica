//
//  UserService.swift
//  Logisitica_iOS
//
//  Created by Feker Hassine on 2020-06-01.
//  Copyright © 2020 Jewel Cheriaa. All rights reserved.
//

import Foundation
import Alamofire
import NVActivityIndicatorView

class UserService
{
    static var authenticatedUser: AuthModel = AuthModel()
    static var carTypeList: [CarTypeModel] = [CarTypeModel]()
    static var userProfile: UserModel = UserModel()
    
    static func verifyPhone (phone: String, completion: @escaping SuccessCompletionHandler)
    {
        let parameters: [String: String] = [
            "userPhone": phone
        ]
        
        GRP.performRequest(withLogs: true,
                           loaderType: .ballDoubleBounce,
                           withSuccesToast: true,
                           successToastMessage: "CodeHasBeenSent",
                           endPoint: "auth/verifPhone",
                           body: parameters,
                           encoding: .JSONEncoding)
        { (error : ErrorModel?) in
            
            completion(error?.success == true)
        }
    }
    
    static func refreshToken (completion: @escaping SuccessCompletionHandler)
    {
        let parameters: [String: String] = [
            "refresh_token": "def502002bd4143c84f252c6cb06fea3fca48b8d0ed9c9fbc8d2846b7b11301410631be865d12a659b0191646738715ad4f314877d3372e57762b25a77fe832d4a27f36158879e6ab873b9f0eec09533d1beea80c6288cb973449b3c195968f374073af865d7b77871a406f75d072863d215d98f362ecdffc6549b4516de9d337bcaf6b97f32b6bfcdda9e4955609a53ea5d760d05b05260f4a5f0c62e54c63dc3f23df87b770c7710eebeb6b4cb6aa7f95dfe8b17f1ffa6f4175a8d8526a071de0c694d9c3f7100f10cb2451ba235aad75a99e473f8ef75d62f20fc527180b2a5ecccc3a95035b011c1ca0437331f90411800397159958b17c73e17cb00934056a6376406718b28ed9afc23872ba341584cb762d9c985663b11e26215471fc33060cb1999a84b9f0835c55f94c702530ff411223d5ccf21abc63a5f6d1961a168981734c742f5e52409b7e19c1a73a9238cbfc9c5b0cc3c2f1e5b5cd59e7191a0"
        ]
        
        GRP.performRequest(withLogs: true,
                           endPoint: "auth/refresh",
                           body: parameters,
                           encoding: .JSONEncoding)
        { (error : ErrorModel?) in
            
            completion(error?.success == true)
        }
    }
    
    
    static func verifyCode(phone: String, code: String, completion: @escaping SuccessCompletionHandler)
    {
        let parameters: [String: String] =
            [
                "userPhone": phone,
                "verificationCode": code
        ]
        
        GRP.performRequest(withLogs: true,
                           loaderType: .ballDoubleBounce,
                           withSuccesToast: true,
                           successToastMessage: "CodeHasBeenSent",
                           endPoint: "auth/verifCode",
                           body: parameters,
                           encoding: .JSONEncoding)
        { (response : SuccessfulResponseModel<[AuthModel]>?) in
            
            guard let response = response, let authResult = response.response, authResult.count > 0, let authObject = authResult.first else
            {
                completion(false)
                GRP.showToast(failure: true, message: "")
                return
            }
            
            authenticatedUser = authObject
            completion(true)
        }
    }
    
    static func register(completion: @escaping SuccessCompletionHandler)
    {
        
        let parameters: [String: Any] =
            [
                "firstName": NewUser.parameters.firstName,
                "lastName": NewUser.parameters.lastName ,
                "email": NewUser.parameters.email,
                "car_type": NewUser.parameters.carType,
                "userPhone": NewUser.parameters.phoneNumber
        ]
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            parameters.forEach { (key, value) in
                let stringValue = "\(value)"
                
                multipartFormData.append(stringValue.data(using: String.Encoding.utf8) ?? Data(), withName: key)
            }
            
            if let identity1 = NewUser.parameters.identity1
            {
                multipartFormData.append(identity1, withName: "identity1", fileName: "identity1.jpeg", mimeType: "image/jpeg")
            }
            if let identity2 = NewUser.parameters.identity2
            {
                multipartFormData.append(identity2, withName: "identity2", fileName: "identity2.jpeg", mimeType: "image/jpeg")
            }
            if let car1 = NewUser.parameters.car1
            {
                multipartFormData.append(car1, withName: "car1", fileName: "car1.jpeg", mimeType: "image/jpeg")
            }
            if let car2 = NewUser.parameters.car2
            {
                multipartFormData.append(car2, withName: "car2", fileName: "car2.jpeg", mimeType: "image/jpeg")
            }
            if let car3 = NewUser.parameters.car3
            {
                multipartFormData.append(car3, withName: "car3", fileName: "car3.jpeg", mimeType: "image/jpeg")
            }
            if let licence1 = NewUser.parameters.licence1
            {
                multipartFormData.append(licence1, withName: "licence1", fileName: "licence1.jpeg", mimeType: "image/jpeg")
            }
            if let licence2 = NewUser.parameters.licence2
            {
                multipartFormData.append(licence2, withName: "licence2", fileName: "licence2.jpeg", mimeType: "image/jpeg")
            }
            
        }, to: BASE_URL+"driver/auth/register", method: .post, encodingCompletion: { result in
            showCustomLoader(message: "Registering".localizedString)
            switch result
            {
                
            case .success(let request, _,  _):
                
                request.responseJSON(completionHandler: { (response) in
                    
                    print(response)
                    print(response.result)
                    
                    hideCustomLoader()
                    
                    guard let userData = response.data else
                    {
                        GRP.showToast(failure: true, message: "RegistrationFailed".localizedString)
                        completion(false)
                        return
                    }
                    
                    guard
                        let newUserContainer = try? JSONDecoder().decode(SuccessfulResponseModel<[AuthModel]>.self, from: userData),
                        let authenticatedUserArray = newUserContainer.response, !authenticatedUserArray.isEmpty,
                        let authenticatedUser = authenticatedUserArray.first,
                        let success = newUserContainer.success, success == true
                        else
                    {
                        guard
                            let errorResponse = try? JSONDecoder().decode(ErrorModel.self, from: userData),
                            let errorMesssage = errorResponse.message else
                        {
                            GRP.showToast(failure: true, message: "RegistrationFailed".localizedString)
                            completion(false)
                            return
                        }
                        GRP.showToast(failure: true, message: errorMesssage)
                        return
                    }
                    self.authenticatedUser = authenticatedUser
                    GRP.showToast(message: "RegistrationSucceeded".localizedString)
                    completion(true)
                })
                
            case .failure:
                hideCustomLoader()
                
                GRP.showToast(failure: true, message: "RegistrationFailed".localizedString)
                completion(false)
                break
            }
        })
        
    }
    
    static func getListCarsType(completion: @escaping SuccessCompletionHandler)
    {
        
        GRP.performRequest(
            withLogs: true,
            loaderType: .ballDoubleBounce,
            withSuccesToast: false,
            endPoint: "listcar",
            method: .get,
            body: nil,
            encoding: .URLEncoding
            )
        {  (carTypeResponse : SuccessfulResponseModel<[CarTypeModel]>?) in
            
            guard let unwrapedReponse = carTypeResponse, let carTypeResult = unwrapedReponse.response, carTypeResult.count > 0 else
            {
                completion(false)
                GRP.showToast(failure: true, message: "Failed".localizedString)
                return
            }
            carTypeList = carTypeResult
            completion(true)
        }
    }
    
    static func getDriverProfile(completion: @escaping SuccessCompletionHandler)
    {
        guard let token = authenticatedUser.accessToken else { completion(false); return}
        
        
        GRP.performRequest(
            withLogs: true,
            loaderType: .ballDoubleBounce,
            withSuccesToast: true,
            //            successToastMessage: "GettingUserProfile".localizedString,
            endPoint: "driver/profile",
            method: .get,
            body: nil,
            encoding: .URLEncoding,
            bearerToken: token
            )
        {  (userProfileResponse : SuccessfulResponseModel<[UserModel]>?) in
            
            guard let unwrapedReponse = userProfileResponse, let userProfileResult = unwrapedReponse.response, userProfileResult.count > 0, let user = userProfileResult.first  else
            {
                completion(false)
                GRP.showToast(failure: true, message: "Failed".localizedString)
                return
            }
            userProfile = user
            completion(true)
        }
    }
    
    static func updateProfile(firstName: String, lastName: String, carType: String, completion: @escaping SuccessCompletionHandler)
    {
        let auth = authenticatedUser
        
        let parameters: [String: Any] =
            [
                "firstName": firstName,
                "lastName": lastName,
                "car_type": carType
        ]
        
        GRP.performRequest(withLogs: true,
                           loaderType: .ballDoubleBounce,
                           withSuccesToast: true,
                           successToastMessage: "ProfileUpdated".localizedString,
                           endPoint: "driver/profile/update",
                           method: .put,
                           body: parameters,
                           encoding: .JSONEncoding,
                           bearerToken: auth.accessToken)
        {  (authResponse : SuccessfulResponseModel<[UserModel]>?) in
            
            guard let unwrapedReponse = authResponse, let authResult = unwrapedReponse.response, authResult.count > 0 else { completion(false) ; return }
            
            authenticatedUser.user = authResult.first
            completion(true)
        }
    }
    static func uploadProfileImage(photo: String, completion: @escaping SuccessCompletionHandler)
    {
        let auth = UserService.authenticatedUser
        
        let parameters: [String: String] =
            [
                "photo": "data:image/jpeg;base64,"+photo
        ]
        
        GRP.performRequest(withLogs: true,
                           loaderType: .ballDoubleBounce,
                           withSuccesToast: true,
                           successToastMessage: "ProfileUpdated".localizedString,
                           endPoint: "user/profileImage",
                           method: .post,
                           body: parameters,
                           encoding: .JSONEncoding,
                           bearerToken: auth.accessToken)
        {  (authResponse : SuccessfulResponseModel<[UserModel]>?) in
            
            guard
                let unwrapedReponse = authResponse,
                let authResult = unwrapedReponse.response,
                authResult.count > 0,
                let userObject = authResult.first
                else
            { completion(false) ; return }
            UserService.authenticatedUser.user?.imageURL = userObject.imageURL
            completion(true)
        }
    }
    
    
    static func postDocumentProfile(type: String, photo: String, completion: @escaping SuccessCompletionHandler)
    {
        let auth = UserService.authenticatedUser
               
               let parameters: [String: String] =
                   [
                        "type": type,
                        "document": "data:image/jpeg;base64,"+photo
               ]
               
               GRP.performRequest(withLogs: true,
                                  loaderType: .ballDoubleBounce,
                                  withSuccesToast: true,
                                  successToastMessage: "ProfileUpdated".localizedString,
                                  endPoint: "driver/document/upload",
                                  method: .post,
                                  body: parameters,
                                  encoding: .JSONEncoding,
                                  bearerToken: auth.accessToken)
               {  (authResponse : SuccessfulResponseModel<[UserModel]>?) in
                   
                   guard
                       let unwrapedReponse = authResponse,
                       let authResult = unwrapedReponse.response,
                       authResult.count > 0,
                       let userObject = authResult.first
                       else
                   { completion(false) ; return }
                   completion(true)
               }
    }
}

// MARK: - AuthModel
struct AuthModel: Decodable {
    var tokenType: String?
    var expiresIn: Double?
    var accessToken, refreshToken: String?
    var expirationDate: Double?
    var user: UserModel?
    var isAlreadyUser: Bool?
    
    enum CodingKeys: String, CodingKey {
        case tokenType = "token_type"
        case expiresIn = "expires_in"
        case accessToken = "access_token"
        case refreshToken = "refresh_token"
        case expirationDate = "expiration_date"
        case user, isAlreadyUser
    }
}

// MARK: - UserModel
struct UserModel: Decodable {
    var id: Int?
    var firstName, lastName, email, phone: String?
    var imageURL: String?
    var car: CarTypeModel?
    var attachements: Attachements?
    var account: Account?
    
    enum CodingKeys: String, CodingKey {
        case id, firstName, lastName, email, phone
        case imageURL = "image_url"
        case car, attachements
        
    }
}

// MARK: - Attachements
struct Attachements: Decodable {
    var identity, carPhoto, licence: [DocumentModel]
    
    enum CodingKeys: String, CodingKey {
        case identity
        case carPhoto = "car_photo"
        case licence
    }
}

// MARK: - DocumentModel
struct DocumentModel: Decodable {
    var path: String
    var id: Int
}

// MARK: - Account
struct Account: Codable {
    let balance, userID: Int
    
    enum CodingKeys: String, CodingKey {
        case balance
        case userID = "user_id"
    }
}

// MARK: - CarTypeModel
struct CarTypeModel: Decodable {
    var id: Int?
    var model: MultiLanguageModel
    var price, capacity: Int?
    var image: String?
    var rangeLuggage: String?
    
    enum CodingKeys: String, CodingKey {
        case id, model, price, capacity, image
        case rangeLuggage = "range_luggage"
    }
}

// MARK: - MultiLanguageModel
struct MultiLanguageModel: Decodable {
    var en, ar: String
}

func showCustomLoader(message: String)
{
    DispatchQueue.main.async {
        guard !NVActivityIndicatorPresenter.sharedInstance.isAnimating else { return }
        let activityData = ActivityData(message: message, messageFont: UIFont(name: "Objective-Regular", size: 17), type: .circleStrokeSpin, color: .white)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
}

func hideCustomLoader()
{
    DispatchQueue.main.async {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
}
