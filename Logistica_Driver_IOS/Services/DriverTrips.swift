//
//  DriverTrips.swift
//  Logistica_Driver_IOS
//
//  Created by Ghazi Tozri on 6/22/20.
//  Copyright © 2020 wimobi. All rights reserved.
//
import Foundation
import Alamofire
import NVActivityIndicatorView

class DriverTrips
{
    static var userProfile: UserModel = UserModel()
    static var authUserTrips: AllTripsData = AllTripsData()
    static var page: Int = 1
    static var isLoading = false
    static var finishPagination = false

    
    static func getTripRequests(completion: @escaping SuccessCompletionHandler){
        
        guard !isLoading, let token = UserService.authenticatedUser.accessToken else { completion(false); return}
        isLoading = true
        
        GRP.performRequest(
            withLogs: true,
            loaderType: .ballDoubleBounce,
            withSuccesToast: true,
            successToastMessage: "GettingTrips".localizedString,
            endPoint: "driver/trip/search?key=0&page=\(page)",
            method: .get,
            body: nil,
            encoding: .URLEncoding,
            bearerToken: token
            )
        {  (userProfileResponse : SuccessfulResponseModel<[AllTripsData]>?) in
            
            guard let unwrapedReponse = userProfileResponse, let userTripsResult = unwrapedReponse.response, userTripsResult.count > 0, let trips = userTripsResult.first, let tripsData = trips.data else
            {
                isLoading = false
                completion(false)
                GRP.showToast(failure: true, message: "Failed".localizedString)
                return
            }
            isLoading = false
            authUserTrips.data?.append(contentsOf: tripsData)
            if (page == authUserTrips.lastPage)
            {
                GRP.showToast(message: "NoMoreTrips".localizedString)
                finishPagination = true
                completion(true)
            }
            else
            {
                page += 1
                completion(true)
            }
        }
    }
    
    static func getListOfTrips(completion: @escaping SuccessCompletionHandler)
        {
            guard let token = UserService.authenticatedUser.accessToken else { completion(false); return}
            
            
            GRP.performRequest(
                withLogs: true,
                loaderType: .ballDoubleBounce,
                withSuccesToast: true,
                successToastMessage: "GettingTrips".localizedString,
                endPoint: "driver/trip/list",
                method: .get,
                body: nil,
                encoding: .URLEncoding,
                bearerToken: token
                )
            {  (userProfileResponse : SuccessfulResponseModel<[AllTripsData]>?) in
                
                guard let unwrapedReponse = userProfileResponse, let userTripsResult = unwrapedReponse.response, userTripsResult.count > 0, let trips = userTripsResult.first  else
                {
                    completion(false)
                    GRP.showToast(failure: true, message: "Failed".localizedString)
                    return
                }
                authUserTrips = trips
                completion(true)
            }
        }
    }



// MARK: - AllTripsData
struct AllTripsData: Decodable {
    var currentPage: Int?
    var data: [TripModel]?
    var firstPageURL: String?
    var from, lastPage: Int?
    var lastPageURL: String?
    var nextPageURL: String?
    var path: String?
    var perPage: Int?
    var prevPageURL: String?
    var to, total: Int?

    enum CodingKeys: String, CodingKey {
        case currentPage = "current_page"
        case data
        case firstPageURL = "first_page_url"
        case from
        case lastPage = "last_page"
        case lastPageURL = "last_page_url"
        case nextPageURL = "next_page_url"
        case path
        case perPage = "per_page"
        case prevPageURL = "prev_page_url"
        case to, total
    }
}

// MARK: - TripModel
struct TripModel: Decodable {
    var id, pickupAt, totalPrice: Int?
    var user: UserModel?
    var addresses: [Address]?

    enum CodingKeys: String, CodingKey {
        case id
        case pickupAt = "pickup_at"
        case totalPrice = "total_price"
        case user, addresses
    }
}

// MARK: - Address
struct Address: Codable {
    var id: Int?
    var primaryName, secondaryName: String?
    var placeID: String?
    var type: String?
    var longitude, lattitude: Double?

    enum CodingKeys: String, CodingKey {
        case id, primaryName, secondaryName
        case placeID = "place_id"
        case type, longitude, lattitude
    }
}
