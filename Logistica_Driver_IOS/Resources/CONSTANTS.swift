//
//  CONSTANTS.swift
//  Logistica_Driver_IOS
//
//  Created by Jewel Cheriaa on 6/10/20.
//  Copyright © 2020 wimobi. All rights reserved.
//

import Foundation


let LOGIN_STORYBORAD        = "Login"
let HOME_STORYBORAD         = "Home"
let MENU_STORYBORAD         = "Menu"
let SETTINGS_STORYBORAD     = "Settings"


typealias SuccessCompletionHandler = (_ Success: Bool) -> ()



let GOOGLE_API_KEY                  = "AIzaSyCntQH3LirilP1aq2PSxgRCRsf_-o2fPtI"



let BASE_URL                        = "https://logistica.wi-mobi.com/api/"

let VERIF_PHONE_URL                 = "auth/verifPhone"
let VERIF_CODE_URL                  = "auth/verifCode"
let REGISTER_URL                    = "auth/register"
let UPDATE_PROFILE_URL              = "user"

let FETCH_NOTIFICATIONS_URL         = "notifs/list"

let FETCH_CARS_URL                  = "services/car_categories"
let CHECK_PROMO_CODE_URL            = "promocode/verify"
let FETCH_SERVICES_LIST_URL         = "services/list_services"
let ADD_DOCUMENT_URL                = "trip/document"

let FETCH_ALL_TRIPS_URL             = "trip/list"
let FETCH_NEXT_PAGE_TRIPS_URL       = "trip/search?"
let FETCH_SINGLE_TRIP_URL           = "trip/"
let CANCEL_TRIP_URL                 = "trip/cancel"
let RATE_TRIP_URL                   = "trip/rate"

