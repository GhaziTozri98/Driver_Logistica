//
//  AppDelegate.swift
//  Logistica_Driver_IOS
//
//  Created by Wimobi on 6/5/20.
//  Copyright © 2020 wimobi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        
        setupWindow()
        
        configureAppForDarkMode()
        
        configureIQKeyboard()
        
        configureGooleMaps()
        
        setupGenereicRequestPerformer()
        
        return true
    }
}


extension AppDelegate
{
    fileprivate func setupWindow()
    {
        LanguageManager.setupLanguageRequirements()
        
        let navC = UINavigationController(rootViewController: WelcomeController.instantiateFromStoryboard(LOGIN_STORYBORAD))
//        let navC = UINavigationController(rootViewController: HomeController.instantiateFromStoryboard(HOME_STORYBORAD))
        navC.isNavigationBarHidden = true
        window?.rootViewController = navC
        window?.makeKeyAndVisible()
    }
    
    fileprivate func configureAppForDarkMode()
    {
        if #available(iOS 13.0, *) { window?.overrideUserInterfaceStyle = .light }
    }
    
    fileprivate func configureGooleMaps()
    {
        GMSServices.provideAPIKey(GOOGLE_API_KEY)
        GMSPlacesClient.provideAPIKey(GOOGLE_API_KEY)
    }
    
    func configureIQKeyboard()
    {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    
    fileprivate func setupGenereicRequestPerformer()
    {
        GRP.baseApiUrl = BASE_URL
        GRP.preferedFontName = "Axiforma-Bold"
        GRP.defaultLoaderType = .audioEqualizer
    }
}


