//
//  LogoutController.swift
//  Logistica_Driver_IOS
//
//  Created by Ghazi Tozri on 6/22/20.
//  Copyright © 2020 wimobi. All rights reserved.
//

import UIKit

class LogoutController: UIViewController {

    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    @IBAction func Logout(_ sender: Any)
    {
          guard
                    let appDelegate = UIApplication.shared.delegate as? AppDelegate,
                    let keyWindow = appDelegate.window else
                { return }
        
        
                let getStartedVC = WelcomeController.instantiateFromStoryboard(LOGIN_STORYBORAD)
        
                let navigationController = UINavigationController(rootViewController: getStartedVC)
                navigationController.isNavigationBarHidden = true
                keyWindow.rootViewController = navigationController
        
        
                keyWindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
                UIView.transition(with: keyWindow, duration: 0.5, options: .transitionCurlUp , animations: nil, completion: nil)
            }
    
    @IBAction func `return`(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
