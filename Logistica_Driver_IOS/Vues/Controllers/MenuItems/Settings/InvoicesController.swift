//
//  InvoicesController.swift
//  Logistica_Driver_IOS
//
//  Created by Hassine Feker on 2020-06-10.
//  Copyright © 2020 wimobi. All rights reserved.
//

import UIKit

class InvoicesController: UIViewController
{
    let price = 30.0
    
    @IBOutlet weak var cashTableView: ContentSizedTableView!
    @IBOutlet weak var paymentTableView: ContentSizedTableView!
    @IBOutlet weak var cashView: UIView!
    @IBOutlet weak var paymentView: UIView!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setupUI()
    }
    
    func setupUI()
    {
        cashView.layer.cornerRadius = 8
        paymentView.layer.cornerRadius = 8
        cashView.dropCardShadow()
        paymentView.dropCardShadow()
    }
    
    //MARK: - Actions
    
    @IBAction func backButtonTapped(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
    }
}

extension InvoicesController: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView.tag == 0
        {
            return 2
        }
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView.tag == 0
        {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: InvoiceCell.reuseIdentifier) as? InvoiceCell else { return UITableViewCell()
            }
            switch indexPath.row
            {
            case 0:
                cell.cashImageView.image = UIImage(named: "car")
                cell.titleLabel.text = "AllTrips".localizedString
                cell.detailsLabel.text = "AllMonetaryValue".localizedString
                cell.priceLabel.text = String.localizedStringWithFormat("Credit".localizedString, price)
            case 1:
                cell.cashImageView.image = UIImage(named: "money_icon")
                cell.titleLabel.text = "CompanyCommission".localizedString
                cell.detailsLabel.text = "ValueWithdrawed".localizedString
                cell.priceLabel.text = String.localizedStringWithFormat("Credit".localizedString, price)
            default:
                cell.cashImageView.image = UIImage(named: "car")
                cell.titleLabel.text = "AllTrips".localizedString
                cell.detailsLabel.text = "AllMonetaryValue".localizedString
                cell.priceLabel.text = String.localizedStringWithFormat("Credit".localizedString, price)
            }
            return cell
        }
        else
        {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: InvoiceCell.reuseIdentifier) as? InvoiceCell else { return UITableViewCell()
            }
            switch indexPath.row
            {
            case 0:
                cell.cashImageView.image = UIImage(named: "car")
                cell.titleLabel.text = "AllTrips".localizedString
                cell.detailsLabel.text = "AllMonetaryValueElectronic".localizedString
                cell.priceLabel.text = String.localizedStringWithFormat("Credit".localizedString, price)
            case 1:
                cell.cashImageView.image = UIImage(named: "money_icon")
                cell.titleLabel.text = "CompanyToPay".localizedString
                cell.detailsLabel.text = "ValueToPay".localizedString
                cell.priceLabel.text = String.localizedStringWithFormat("Credit".localizedString, price)
                
            case 2:
                cell.cashImageView.image = UIImage(named: "money_icon")
                cell.titleLabel.text = "Paid".localizedString
                cell.detailsLabel.text = "CreditsPaid".localizedString
                cell.priceLabel.text = String.localizedStringWithFormat("Credit".localizedString, price)
                
            case 3:
                cell.cashImageView.image = UIImage(named: "money_icon")
                cell.titleLabel.text = "Rest".localizedString
                cell.detailsLabel.text = "RestToPay".localizedString
                cell.priceLabel.text = String.localizedStringWithFormat("Credit".localizedString, price)
            default:
                cell.cashImageView.image = UIImage(named: "car")
                cell.titleLabel.text = "AllTrips".localizedString
                cell.detailsLabel.text = "AllMonetaryValue".localizedString
                cell.priceLabel.text = String.localizedStringWithFormat("Credit".localizedString, price)
            }
            return cell
        }
    }
    
    
}
