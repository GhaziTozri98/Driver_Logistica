//
//  PaymentController.swift
//  Logistica_Driver_IOS
//
//  Created by Jewel Cheriaa on 6/10/20.
//  Copyright © 2020 wimobi. All rights reserved.
//

import UIKit

class PaymentController: BaseController
{
    
    @IBOutlet var paymentButtons: [UIButton]!
    
    @IBOutlet weak var couponTextField: UITextField!
    @IBOutlet weak var currentBalanceTextField: UITextField!
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        currentBalanceTextField.placeholder = "CurrentBalance".localizedString
        
    }
    
    @IBAction func paymentPick(_ sender: UIButton)
    {
        guard let Buttons = paymentButtons else { return }
        for (idx, button) in Buttons.enumerated()
        {
            if sender.tag == idx
            {
                button.setImage(UIImage(named: "radioButton-special-checked" ), for: .normal)
            }
            else
            {
                button.setImage(UIImage(named: "checkboxEmpty" ), for: .normal)
            }
        }
    }
    
    
    @IBAction func continueButton(_ sender: Any)
    {
    }
    
}
