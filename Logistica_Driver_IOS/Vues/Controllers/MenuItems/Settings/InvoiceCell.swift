//
//  InvoiceCell.swift
//  Logistica_Driver_IOS
//
//  Created by Feker Hassine on 2020-06-10.
//  Copyright © 2020 wimobi. All rights reserved.
//

import UIKit

class InvoiceCell: UITableViewCell
{
    @IBOutlet weak var cashImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
}
