//
//  SettingsController.swift
//  Logistica_Driver_IOS
//
//  Created by Jewel Cheriaa on 6/10/20.
//  Copyright © 2020 wimobi. All rights reserved.
//

import UIKit
import AVFoundation
import DropDown
import SDWebImage


class SettingsController: MenuItemController
{
    
    var destination = 0 // from 0 to 6, -1 for profile Picture
    var dropDown = DropDown()
    let price = 200
    var editionActivated = false
    var carType: Int = 0
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileImageButton: UIButton!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var typeOfCarContainerView: UIView!
    @IBOutlet weak var typeOfCarTextField: UITextField!
    @IBOutlet weak var accountBalanceLabel: UILabel!
    @IBOutlet var photosButtons: [UIButton]!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var editCarInfoButton: UIButton!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setupUI()
        setupData()
        getDriverPhotos()
    }
    
    @IBAction func openPhotoPickingSelector(_ sender: UIButton)
    {
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized
        {
            preparePickerController(destination: sender.tag)
        }
        else
        {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { [weak self] granted in
                guard granted else
                {
                    //                           self?.showDeniedCameraAccessAlert()
                    return
                }
                self?.preparePickerController(destination: sender.tag)
            })
        }
    }
    
    func preparePickerController(destination: Int)
    {
        DispatchQueue.main.async { [weak self] in
            self?.destination = destination
            let pickerSourceController = PickerSourceController.instantiateFromStoryboard(SETTINGS_STORYBORAD)
            pickerSourceController.delegate = self
            self?.present(pickerSourceController, animated: true, completion: nil)
        }
    }
    
    @IBAction func editSaveAction(_ sender: Any)
    {
        
        editionActivated.toggle()
        guard
            let firstName = self.firstNameTextField.text,
            let lastName = self.lastNameTextField.text
            //            let carType = self.dropDown.indexPathForSelectedRow
            else {return}
        if !editionActivated
        {
            UserService.updateProfile(firstName: firstName, lastName: lastName, carType: String(carType)) { (success) in
            }
        }
        self.saveButton.setTitle( self.editionActivated ? "Save".localizedString :"Edit".localizedString, for: .normal)
        self.firstNameTextField.isUserInteractionEnabled.toggle()
        self.lastNameTextField.isUserInteractionEnabled.toggle()
        self.editCarInfoButton.isUserInteractionEnabled.toggle()
        
        
        
    }
    
    @IBAction func selectCarType(_ sender: Any)
    {
        DropDown.startListeningToKeyboard()
        dropDown.show()
        dropDown.selectionAction = { (index: Int, item: String) in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.typeOfCarTextField.text = item
                self.carType = index + 1
            }
        }
    }
    
    @IBAction func logout(_ sender: Any)
    {
        
        let signoutModal = LogoutController.instantiateFromStoryboard(SETTINGS_STORYBORAD)
        navigationController?.present(signoutModal, animated: true, completion: nil)
    }
}


//MARK: - HELPERS

extension SettingsController
{
    
    func setupData()
    {
        UserService.getListCarsType { (success) in
            guard success else { return }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.configureDropDown()
            }
        }
    }
    
    func setupUI()
    {
        UserService.getDriverProfile { (success) in
            guard success else { return }
            DispatchQueue.main.async { [weak self] in
                guard let self = self
                    else { return }
                self.firstNameTextField.text = UserService.authenticatedUser.user?.firstName
                self.lastNameTextField.text = UserService.authenticatedUser.user?.lastName
                self.emailTextField.text = UserService.authenticatedUser.user?.email
                self.mobileTextField.text = UserService.authenticatedUser.user?.phone
                self.typeOfCarTextField.placeholder = USING_ENGLISH ? UserService.authenticatedUser.user?.car?.model.en : UserService.authenticatedUser.user?.car?.model.ar ?? UserService.authenticatedUser.user?.car?.model.en
                self.carType = UserService.authenticatedUser.user?.car?.id as Int? ?? 1
                //Setting images
                if let userImageURL = UserService.userProfile.imageURL
                {
                    self.profileImageButton.activateSdWebImageLoader()
                    self.profileImageButton.sd_setImage(with: URL(string: userImageURL), for: .normal)
                }
                else
                {
                    self.profileImageButton.setPlaceholder(placeholderImage: .profile)
                }
                if let balanceContainer = UserService.authenticatedUser.user?.account?.balance
                {
                    self.accountBalanceLabel.text = String(balanceContainer)
                }
                else
                {
                    self.accountBalanceLabel.text = String.localizedStringWithFormat("Credit".localizedString, self.price)
                }
            }
        }
    }
    
    func getDriverPhotos()
    {
        guard let userObject = UserService.authenticatedUser.user, let attachments = userObject.attachements else
        { return }
        for (idx,photoObject) in attachments.identity.enumerated()
        {
            self.photosButtons[idx].sd_setImage(with: URL(string: photoObject.path ), for: .normal)
        }
        for (idx,photoObject) in attachments.carPhoto.enumerated()
        {
            self.photosButtons[idx+2].sd_setImage(with: URL(string: photoObject.path ), for: .normal)
        }
        for (idx,photoObject) in attachments.licence.enumerated()
        {
            self.photosButtons[idx+5].sd_setImage(with: URL(string: photoObject.path ), for: .normal)
        }
    }
    
    fileprivate func configureDropDown()
    {
        dropDown.anchorView = typeOfCarContainerView
        DropDown.appearance().textFont = UIFont(name: "Axiforma-R", size: 14)  ?? UIFont.systemFont(ofSize: 13)
        for car in UserService.carTypeList
        {
            dropDown.dataSource.append(USING_ENGLISH ? car.model.en : car.model.ar )
        }
    }
    
}


extension SettingsController: PhotoChoosingDelegate
{
    func openPicker(sourceType: UIImagePickerController.SourceType)
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = sourceType
            self?.present(imagePicker, animated: true, completion: nil)
        }
    }
}

//MARK: - UIImagePickerControllerDelegate

extension SettingsController: UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        guard
            let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage,
            let jpegData = pickedImage.jpegData(compressionQuality: 0.1),
            let compressedImage = UIImage(data: jpegData)
            else
        {
            picker.dismiss(animated: true,completion: nil)
            return
        }
        
        picker.dismiss(animated: true) { [weak self] in
            guard let self = self else { return }
            guard self.destination >= -1 , self.destination <= 6 else { return }
            let base64EncodedImage = jpegData.base64EncodedString()
            switch self.destination
            {
            case -1:
                //Profile Picture
                UserService.uploadProfileImage(photo: base64EncodedImage){ (success) in
                    guard success else {return}
                    DispatchQueue.main.async (){ [weak self] in
                        guard let self = self else {return}
                        self.profileImageButton.setImage(compressedImage, for: .normal)
                    }
                }
                
            case 0,1:
                UserService.postDocumentProfile(type: "4", photo: base64EncodedImage){ (success) in
                    guard success else {return}
                    DispatchQueue.main.async (){ [weak self] in
                        guard let self = self else {return}
                        self.photosButtons[self.destination].setImage(compressedImage, for: .normal)
                    }
                }
                self.setupUI()
                
            case 2,3,4:
                UserService.postDocumentProfile(type: "5", photo: base64EncodedImage){ (success) in
                    guard success else {return}
                    DispatchQueue.main.async (){ [weak self] in
                        guard let self = self else {return}
                        self.photosButtons[self.destination].setImage(compressedImage, for: .normal)
                    }
                }
                self.setupUI()
                
            case 5,6:
                UserService.postDocumentProfile(type: "6", photo: base64EncodedImage){ (success) in
                    guard success else {return}
                    DispatchQueue.main.async (){ [weak self] in
                        guard let self = self else {return}
                        self.photosButtons[self.destination].setImage(compressedImage, for: .normal)
                    }
                }
                self.setupUI()
                
            default:
                break
            }
            self.photosButtons[self.destination].setImage(compressedImage, for: .normal)
        }
    }
}
