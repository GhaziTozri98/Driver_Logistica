//
//  PickerSourceController.swift
//  ByArabs
//
//  Created by Mohamed Ali BELHADJ on 26/03/2020.
//  Copyright © 2020 Creativity Echo. All rights reserved.
//

import UIKit

protocol PhotoChoosingDelegate: AnyObject
{
    func openPicker(sourceType: UIImagePickerController.SourceType)
}

class PickerSourceController: PopupController
{
    override var popupHeight: CGFloat { return 200.0 }
    
    weak var delegate: PhotoChoosingDelegate?
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
    }
    
    
    
    @IBAction func openCamera(_ sender: Any)
    {
        delegate?.openPicker(sourceType: .camera)
        super.closePopup(nil)
    }
    
    @IBAction func openLibrayPhoto(_ sender: Any)
    {
        delegate?.openPicker(sourceType: .photoLibrary)
        super.closePopup(nil)
    }
}
