//
//  ReviewsController.swift
//  Logistica_Driver_IOS
//
//  Created by Jewel Cheriaa on 6/10/20.
//  Copyright © 2020 wimobi. All rights reserved.
//

import UIKit

class ReviewsController: BaseController
{
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
    }
    
}



extension ReviewsController: UITableViewDataSource, UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReviewCell.reuseIdentifier, for: indexPath) as? ReviewCell else { return UITableViewCell() }
        
        return cell
    }
}


class ReviewCell: UITableViewCell
{
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var reviewContentLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet var notationStars: [UIButton]!
    
}
