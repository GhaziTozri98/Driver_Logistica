//
//  OrdersController.swift
//  Logistica_Driver_IOS
//
//  Created by Jewel Cheriaa on 6/10/20.
//  Copyright © 2020 Jewel Cheriaa. All rights reserved.
//


import UIKit

class OrdersController: MenuItemController
{
    
    @IBOutlet weak var currentButton: UIButton!
    @IBOutlet weak var finishedButton: UIButton!
    @IBOutlet weak var canceledButton: UIButton!
    @IBOutlet weak var sectionIndexViewLeadingConstraint: NSLayoutConstraint!
    
    
    //COLLECTIONS
    @IBOutlet weak var containerCollectionView: UICollectionView!
    
    
    var cellIdentifiers: [String] =
        USING_ENGLISH == true
            ?
            [CurrentOrdersListCell.reuseIdentifier, FinishedOrdersListCell.reuseIdentifier, CanceledOrdersListCell.reuseIdentifier]
            :
            [CanceledOrdersListCell.reuseIdentifier, FinishedOrdersListCell.reuseIdentifier, CurrentOrdersListCell.reuseIdentifier]
    
    
    
    let factor: CGFloat = 1/3
    
    //MARK: - LIFE CYCLE
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setupUI()
        
        setupData()
        
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    func switchToSectionBy(tag selectedTag: Int, animated: Bool = true)
    {
        if !USING_ENGLISH
        {
            if selectedTag == 0
            {
                containerCollectionView.scrollToItem(at: IndexPath(item: 2, section: 0), at: .left, animated: animated)
            }else
            {
                if selectedTag == 2
                {
                    containerCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: animated)
                }else
                {
                    containerCollectionView.scrollToItem(at: IndexPath(item: selectedTag, section: 0), at: .left, animated: animated)
                }
            }
        }else
        {
            containerCollectionView.scrollToItem(at: IndexPath(item: selectedTag, section: 0), at: .left, animated: animated)
        }
    }
    
    
    //MARK: - IBACTIONS
    
    @IBAction func switchSection(_ sender: UIButton)
    {
        switchToSectionBy(tag: sender.tag)
    }
}




//MARK: - HELPERS

extension OrdersController
{
    func setupUI()
    {
        
        if !USING_ENGLISH
        {
            DispatchQueue.main.async {
                self.switchToSectionBy(tag: 0, animated: false)
            }
        }
    }
    
    func setupData()
    {
//        TripService.getAllTrips { (success) in
//            guard success else { return }
//
//            DispatchQueue.main.async {[weak self] in
//                guard let self = self else { return }
//
//                for cellIndex in 0...2
//                {
//                    self.containerCollectionView.reloadItems(at: [IndexPath(item: cellIndex, section: 0)])
//                }
//            }
//        }
    }
}



//MARK: - UICollectionView

extension OrdersController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return cellIdentifiers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
       guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifiers[indexPath.item], for: indexPath) as? OrdersListCell else { return UICollectionViewCell() }
        cell.delegate = self
        cell.reloadTableView()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height - 60)
    }
    
}



//MARK: - SCROLL VIEW DELEGATE

extension OrdersController
{
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        guard  scrollView.tag == 0 else { return }
        
        sectionIndexViewLeadingConstraint.constant = scrollView.contentOffset.x * factor
        
        if scrollView.contentOffset.x == 0
        {
            UIView.transition(with: currentButton, duration: 0.3, options: .transitionFlipFromTop, animations: { [weak self] in
                self?.currentButton.setTitleColor(#colorLiteral(red: 0.1411764706, green: 0.8196078431, blue: 0.6666666667, alpha: 1), for: .normal)
                self?.currentButton.titleLabel?.font = UIFont(name: "Axiforma-Bold", size: 15) ?? UIFont.systemFont(ofSize: 15)
            })
            UIView.transition(with: finishedButton, duration: 0.3, options: .transitionCrossDissolve, animations: { [weak self] in
                self?.finishedButton.setTitleColor(#colorLiteral(red: 0.1411764706, green: 0.1803921569, blue: 0.2588235294, alpha: 1), for: .normal)
                self?.finishedButton.titleLabel?.font = UIFont(name: "Axiforma-Regular", size: 13) ?? UIFont.systemFont(ofSize: 13)
            })
            UIView.transition(with: canceledButton, duration: 0.3, options: .transitionCrossDissolve, animations: { [weak self] in
                self?.canceledButton.setTitleColor(#colorLiteral(red: 0.1411764706, green: 0.1803921569, blue: 0.2588235294, alpha: 1), for: .normal)
                self?.canceledButton.titleLabel?.font = UIFont(name: "Axiforma-Regular", size: 13) ?? UIFont.systemFont(ofSize: 13)
            })
        }
        else if scrollView.contentOffset.x == UIScreen.main.bounds.width
        {
            UIView.transition(with: currentButton, duration: 0.3, options: .transitionCrossDissolve, animations: { [weak self] in
                self?.currentButton.setTitleColor(#colorLiteral(red: 0.1411764706, green: 0.1803921569, blue: 0.2588235294, alpha: 1), for: .normal)
                self?.currentButton.titleLabel?.font = UIFont(name: "Axiforma-Regular", size: 13) ?? UIFont.systemFont(ofSize: 13)
            })
            UIView.transition(with: finishedButton, duration: 0.3, options: .transitionFlipFromTop, animations: { [weak self] in
                self?.finishedButton.setTitleColor(#colorLiteral(red: 0.1411764706, green: 0.8196078431, blue: 0.6666666667, alpha: 1), for: .normal)
                self?.finishedButton.titleLabel?.font = UIFont(name: "Axiforma-Bold", size: 15) ?? UIFont.systemFont(ofSize: 15)
            })
            UIView.transition(with: canceledButton, duration: 0.3, options: .transitionCrossDissolve, animations: { [weak self] in
                self?.canceledButton.setTitleColor(#colorLiteral(red: 0.1411764706, green: 0.1803921569, blue: 0.2588235294, alpha: 1), for: .normal)
                self?.canceledButton.titleLabel?.font = UIFont(name: "Axiforma-Regular", size: 13) ?? UIFont.systemFont(ofSize: 13)
            })
        }
        else if scrollView.contentOffset.x == UIScreen.main.bounds.width * 2
        {
            UIView.transition(with: currentButton, duration: 0.3, options: .transitionCrossDissolve, animations: { [weak self] in
                self?.currentButton.setTitleColor(#colorLiteral(red: 0.1411764706, green: 0.1803921569, blue: 0.2588235294, alpha: 1), for: .normal)
                self?.currentButton.titleLabel?.font = UIFont(name: "Axiforma-Regular", size: 13) ?? UIFont.systemFont(ofSize: 13)
            })
            UIView.transition(with: finishedButton, duration: 0.3, options: .transitionCrossDissolve, animations: { [weak self] in
                self?.finishedButton.setTitleColor(#colorLiteral(red: 0.1411764706, green: 0.1803921569, blue: 0.2588235294, alpha: 1), for: .normal)
                self?.finishedButton.titleLabel?.font = UIFont(name: "Axiforma-Regular", size: 13) ?? UIFont.systemFont(ofSize: 13)
            })
            UIView.transition(with: canceledButton, duration: 0.3, options: .transitionFlipFromTop, animations: { [weak self] in
                self?.canceledButton.setTitleColor(#colorLiteral(red: 0.1411764706, green: 0.8196078431, blue: 0.6666666667, alpha: 1), for: .normal)
                self?.canceledButton.titleLabel?.font = UIFont(name: "Axiforma-Bold", size: 15) ?? UIFont.systemFont(ofSize: 15)
            })
        }
    }
    
}
