//
//  OrdersListCell.swift
//  Logistica_Driver_IOS
//
//  Created by Jewel Cheriaa on 6/10/20.
//  Copyright © 2020 Jewel Cheriaa. All rights reserved.
//

import UIKit

class OrdersListCell: UICollectionViewCell, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var tableView: UITableView!
    
    var delegate: UIViewController?
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
            
        tableView.contentInset = .init(top: 35, left: 0, bottom: 30, right: 0)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return 7

        
//        switch reuseIdentifier
//        {
//            case CurrentOrdersListCell.reuseIdentifier:
//                guard let currentTrips = TripService.allTrips.current else { return 0 }
//                return currentTrips.count
//            case FinishedOrdersListCell.reuseIdentifier:
//                guard let finishedTrips = TripService.allTrips.finished else { return 0 }
//                return finishedTrips.count
//            case CanceledOrdersListCell.reuseIdentifier:
//                guard let canceledTrips = TripService.allTrips.canceled else { return 0 }
//                return canceledTrips.count
//            default:
//                return 0
//        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: OrderCell.reuseIdentifier, for: indexPath) as? OrderCell else { return UITableViewCell() }
        
//        var singleTrip: SingleTripModel = SingleTripModel()
//
//        switch reuseIdentifier
//        {
//            case CurrentTripsListCell.reuseIdentifier:
//                guard indexPath.item < TripService.allTrips.current?.count ?? 0 else {  return UITableViewCell()  }
//
//                if let unwrappedTrip = TripService.allTrips.current?[indexPath.item]
//                {
//                    singleTrip = unwrappedTrip
//                }
//
//            case FinishedTripsListCell.reuseIdentifier:
//                guard indexPath.item < TripService.allTrips.finished?.count ?? 0 else {  return UITableViewCell()  }
//
//                if let unwrappedTrip = TripService.allTrips.finished?[indexPath.item]
//                {
//                    singleTrip = unwrappedTrip
//                }
//            case CanceledTripsListCell.reuseIdentifier:
//                guard indexPath.item < TripService.allTrips.canceled?.count ?? 0 else {  return UITableViewCell()  }
//
//                if let unwrappedTrip = TripService.allTrips.canceled?[indexPath.item]
//                {
//                    singleTrip = unwrappedTrip
//                }
//            default:
//                return UITableViewCell()
//        }
//
//        cell.containerView.dropCardShadow()
//        cell.setupData(trip: singleTrip)
        return cell
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
//    {
//        guard !TripService.tripsPagesAreLoading else { return }
//
//        var categoryId = -1
//
//        switch reuseIdentifier
//        {
//        case CurrentTripsListCell.reuseIdentifier:
//            categoryId = 1
//
//            guard
//                !TripService.currentTripsPageFinished,
//                let currentTripsCount = TripService.allTrips.current?.count,
//                indexPath.item + 1 == currentTripsCount,
//                let visibleIndexPaths = tableView.indexPathsForVisibleRows,
//                visibleIndexPaths.contains(IndexPath(row: currentTripsCount - 1, section: 0))
//            else
//            { return }
//
//            TripService.fetchTripsNextPage (tripCategoryId: categoryId) { [weak self] (success) in
//                guard success, let self = self else { return }
//                self.reloadTableView()
//            }
//
//            break
//        case FinishedTripsListCell.reuseIdentifier:
//            categoryId = 2
//
//            guard
//                !TripService.finishedTripsPageFinished,
//                let finishedTripsCount = TripService.allTrips.finished?.count,
//                indexPath.item + 1 == TripService.allTrips.finished?.count,
//                let visibleIndexPaths = tableView.indexPathsForVisibleRows,
//                visibleIndexPaths.contains(IndexPath(row: finishedTripsCount - 1, section: 0))
//            else
//            { return }
//
//            TripService.fetchTripsNextPage (tripCategoryId: categoryId) { [weak self] (success) in
//                guard success, let self = self else { return }
//                self.reloadTableView()
//            }
//
//            break
//
//        case CanceledTripsListCell.reuseIdentifier:
//            categoryId = 3
//
//            guard
//                !TripService.canceledTripsPageFinished,
//                let canceledTripsCount = TripService.allTrips.finished?.count,
//                indexPath.item + 1 == TripService.allTrips.canceled?.count,
//                let visibleIndexPaths = tableView.indexPathsForVisibleRows,
//                visibleIndexPaths.contains(IndexPath(row: canceledTripsCount - 1, section: 0))
//            else
//            { return }
//
//            TripService.fetchTripsNextPage (tripCategoryId: categoryId) { [weak self] (success) in
//                guard success, let self = self else { return }
//                self.reloadTableView()
//            }
//
//            break
//
//        default:
//
//            break
//        }
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
//    {
//        guard let delegate = delegate else { return }
//        let tripDetailsVC = TripDetailsController.instantiateFromStoryboard(HOME_STORYBOARD_ID)
//        if reuseIdentifier == CurrentTripsListCell.reuseIdentifier
//        {
//            tripDetailsVC.type = .currentTrip
//
//            guard
//                let currentTrips = TripService.allTrips.current,
//                indexPath.item < currentTrips.count,
//                let currentTripID = currentTrips[indexPath.item].id
//            else { return }
//            tripDetailsVC.tripId = currentTripID
//        }else
//        {
//            if reuseIdentifier == FinishedTripsListCell.reuseIdentifier
//            {
//                guard
//                    let finishedTrips = TripService.allTrips.finished,
//                    indexPath.item < finishedTrips.count,
//                    let finishedTripID = finishedTrips[indexPath.item].id
//                else { return }
//                tripDetailsVC.tripId = finishedTripID
//
//                tripDetailsVC.type = .finishedTrip
//            }else
//            {
//                guard
//                    let canceledTrips = TripService.allTrips.canceled,
//                    indexPath.item < canceledTrips.count,
//                    let canceledTripID = canceledTrips[indexPath.item].id
//                else { return }
//                tripDetailsVC.tripId = canceledTripID
//
//                tripDetailsVC.type = .cancelledTrip
//            }
//        }
//        tripDetailsVC.delegate = delegate
//        delegate.navigationController?.pushViewController(tripDetailsVC, animated: true)
//    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tripDetailsController = TripDetailsController.instantiateFromStoryboard(HOME_STORYBORAD)
        self.delegate?.navigationController?.pushViewController(tripDetailsController, animated: true)
    }
    func reloadTableView()
    {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.tableView.reloadData()
        }
    }
    
}

class CurrentOrdersListCell: OrdersListCell {}
class FinishedOrdersListCell: OrdersListCell {}
class CanceledOrdersListCell: OrdersListCell {}


class OrderCell: UITableViewCell
{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var pickUpPrimaryAdressLabel: UILabel!
    @IBOutlet weak var pickUpSecondaryAdressLabel: UILabel!
    @IBOutlet weak var destinationPrimaryAdressLabel: UILabel!
    @IBOutlet weak var destinationSecondaryAdressLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var driverImageView: UIImageView!
    @IBOutlet weak var driverNameLabel: UILabel!
    
    
//    func setupData(trip: SingleTripModel)
//    {
//        guard let pickUpAdress = trip.addresses?.first(where: { (adress) -> Bool in
//            return adress.type == "1"
//        }) else { return }
//        pickUpPrimaryAdressLabel.text = pickUpAdress.primaryName
//        pickUpSecondaryAdressLabel.text = pickUpAdress.secondaryName
//        
//        guard let destinationAdress = trip.addresses?.first(where: { (adress) -> Bool in
//            return adress.type == "2"
//        }) else { return }
//        destinationPrimaryAdressLabel.text = destinationAdress.primaryName
//        destinationSecondaryAdressLabel.text = destinationAdress.secondaryName
//        
//        priceLabel.text = String(trip.totalPrice ?? 0) + CURRENCY
//
//        guard let driver = trip.driver else { return }
//        if let driverPhoto = driver.imageURL
//        {
//            driverImageView.activateSdWebImageLoader()
//            driverImageView.sd_setImage(with: URL(string: driverPhoto), placeholderImage: UIImage(named: "profilePlaceholder"))
//        }
//        else
//        {
//            driverImageView.image = UIImage(named: PlaceholdingImages.blackProfile.rawValue)
//        }
//        
//        guard
//            let firstName = driver.firstName,
//            let lastName = driver.lastName
//        else
//        {
//             return
//        }
//        driverNameLabel.text = firstName + " " + lastName
//    }
    
}

extension UIView
{
    func dropShadow(shadowColor: UIColor = UIColor.black,
                    fillColor: UIColor = UIColor.white,
                    opacity: Float = 0.2,
                    offset: CGSize = CGSize(width: 0.0, height: 1.0),
                    radius: CGFloat = 10) -> CAShapeLayer {
        
        let shadowLayer = CAShapeLayer()
        shadowLayer.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: radius).cgPath
        shadowLayer.fillColor = fillColor.cgColor
        shadowLayer.shadowColor = shadowColor.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = offset
        shadowLayer.shadowOpacity = opacity
        shadowLayer.shadowRadius = radius
        layer.insertSublayer(shadowLayer, at: 0)
        return shadowLayer
    }
}


public extension UIView
{

    func roundView()
    {
        self.layer.cornerRadius = 8;
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.layer.shadowRadius = 2;
        self.layer.shadowOpacity = 0.3;
        self.clipsToBounds = true;
    }

    func roundViewWithBorder(_ borderColor: UIColor?)
    {
        self.layer.borderWidth = 1.0
        self.layer.borderColor = borderColor?.cgColor ?? UIColor.black.cgColor
        self.roundView()
    }

    func circularView(color: CGColor? = nil)
    {
        self.layer.borderWidth = 1.0
        self.layer.masksToBounds = false
        self.layer.borderColor = color ?? UIColor.black.cgColor
        self.layer.cornerRadius = self.frame.size.width/2
        self.clipsToBounds = true
    }

    func circularViewWithBorder(_ borderColor: UIColor)
    {
        self.layer.borderWidth = 1.0
        self.layer.masksToBounds = false
        self.layer.borderColor = borderColor.cgColor
        self.layer.cornerRadius = self.frame.size.width/2
        self.clipsToBounds = true
    }
}

extension UIView {
    
    func setShadowWithCornerRadius(corners : CGFloat){
        
        self.layer.cornerRadius = corners
        
        let shadowPath2 = UIBezierPath(rect: self.bounds)
        
        self.layer.masksToBounds = false
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        
        self.layer.shadowOffset = CGSize(width: CGFloat(1.0), height: CGFloat(3.0))
        
        self.layer.shadowOpacity = 0.5
        
        self.layer.shadowPath = shadowPath2.cgPath
        
        
        
    }
    
}
