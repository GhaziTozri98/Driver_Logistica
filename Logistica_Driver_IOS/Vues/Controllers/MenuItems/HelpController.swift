//
//  HelpController.swift
//  Logistica_Driver_IOS
//
//  Created by Jewel Cheriaa on 6/10/20.
//  Copyright © 2020 wimobi. All rights reserved.
//

import UIKit

class HelpController: MenuItemController
{
    
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
    }
    
}



extension HelpController: UITableViewDataSource, UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HelpCell.reuseIdentifier, for: indexPath) as? HelpCell else { return UITableViewCell() }
        
        return cell
    }
}



class HelpCell: UITableViewCell
{
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
}

