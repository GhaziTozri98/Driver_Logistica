//
//  MenuController.swift
//  Logistica_Driver_IOS
//
//  Created by Jewel Cheriaa on 6/10/20.
//  Copyright © 2020 Jewel Cheriaa. All rights reserved.
//


import UIKit


import SideMenu

class MenuItemController: UIViewController
{
    @IBAction func showSideMenu(_ sender: Any)
    {
        view.endEditing(true)
        
        let menuController = MenuController.instantiateFromStoryboard(MENU_STORYBORAD)
        
        let menuNavigationController = SideMenuNavigationController(rootViewController: menuController)
        menuNavigationController.statusBarEndAlpha = 0
        menuNavigationController.menuWidth = UIScreen.main.bounds.width - 110
        menuNavigationController.navigationBar.isHidden = true
        menuNavigationController.leftSide = USING_ENGLISH
        present(menuNavigationController, animated: true, completion: nil)
    }
}


class MenuController: UIViewController
{
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    
    
    
    //MARK: - LIFE CYCLE
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setupData()
    }
    
    func setupData()
    {
        guard
            let authenticatedUser = UserService.authenticatedUser.user,
            let firstName = authenticatedUser.firstName,
            let lastName = authenticatedUser.lastName
            else { return }
        
        userNameLabel.text = firstName + " " + (lastName.first?.uppercased())! + "."
        
        if let userPhoto = authenticatedUser.imageURL
        {
            profileImageView.activateSdWebImageLoader()
            profileImageView.sd_setImage(with: URL(string: userPhoto), placeholderImage: UIImage(named: "profilePlaceholder"))
        }
        else
        {
            profileImageView.image = UIImage(named: PlaceholdingImages.white.rawValue)
        }
    }
    
    
    
    
    func isNotSelectingCurrentShowedItem(SectionClass: AnyClass) -> Bool
    {
        guard
            let appDelegate = UIApplication.shared.delegate as? AppDelegate,
            let mainWindow = appDelegate.window,
            let rootViewController = mainWindow.rootViewController as? UINavigationController,
            let lastVisibleController = rootViewController.children.last,
            !lastVisibleController.isKind(of: SectionClass.self) else
        {
            return  false
        }
        return true
    }
    
    //MARK: - IBACTIONS
    
    @IBAction func menuItemSelected(_ button: UIButton)
    {
        var destinationController: UIViewController?
        
        switch button.tag
        {
        case 0:
            guard isNotSelectingCurrentShowedItem(SectionClass: HomeController.self)  else
            {
                dismiss(animated: true, completion: nil)
                return
            }
            
            destinationController = HomeController.instantiateFromStoryboard(HOME_STORYBORAD)
            break
        case 1:
            guard isNotSelectingCurrentShowedItem(SectionClass: OrdersController.self)  else
            {
                dismiss(animated: true, completion: nil)
                return
            }
            
            destinationController = OrdersController.instantiateFromStoryboard(MENU_STORYBORAD)
            break
            
        case 2:
            guard isNotSelectingCurrentShowedItem(SectionClass: SettingsController.self)  else
            {
                dismiss(animated: true, completion: nil)
                return
            }
            
            destinationController = SettingsController.instantiateFromStoryboard(SETTINGS_STORYBORAD)
            break
            
        case 3:
            
            let textToShare = "Driver Logisitica"
            guard let image = UIImage(named: "logoGreen") else { return }
            
            let avc = UIActivityViewController(activityItems: [image, textToShare], applicationActivities: nil)
            dismiss(animated: true, completion: {
                DispatchQueue.main.async {
                    UIApplication.topViewController?.present(avc, animated: true, completion: nil)
                }
            })
            break
            
        case 4:
            guard
                let destinationUrl = URL(string: "http://wi-mobi.com/"),
                UIApplication.shared.canOpenURL(destinationUrl) else
            { return }
            
            UIApplication.shared.open(destinationUrl, options: [:], completionHandler: nil)
            
            
            break
            
        case 5:
            guard isNotSelectingCurrentShowedItem(SectionClass: HelpController.self)  else
            {
                dismiss(animated: true, completion: nil)
                return
            }
            
            destinationController = HelpController.instantiateFromStoryboard(MENU_STORYBORAD)
            break
            
        default: break
        }
        
        guard
            let secureDestinationController = destinationController,
            let appDelegate = UIApplication.shared.delegate as? AppDelegate,
            let mainWindow = appDelegate.window,
            let rootViewController = mainWindow.rootViewController as? UINavigationController else
        { return }
        
        rootViewController.setViewControllers([secureDestinationController] , animated: false)
        dismiss(animated: true, completion: nil)
    }
    
}


