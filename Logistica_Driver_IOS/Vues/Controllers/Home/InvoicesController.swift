//
//  InvoicesController.swift
//  Logistica_Driver_IOS
//
//  Created by Hassine Feker on 2020-06-10.
//  Copyright © 2020 wimobi. All rights reserved.
//

import UIKit

class InvoicesController: UIViewController
{
    @IBOutlet weak var cashTableView: ContentSizedTableView!
    @IBOutlet weak var paymentTableView: ContentSizedTableView!
    @IBOutlet weak var cashView: UIView!
    @IBOutlet weak var paymentView: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setupUI()
    }
    
    func setupUI()
    {
        cashView.dropCardShadow()
        paymentView.dropCardShadow()
    }
    
    //MARK: - Actions
    
    @IBAction func backButtonTapped(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
    }
}

extension InvoicesController: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView.tag == 0
        {
            return 2
        }
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView.tag == 0
        {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: InvoiceCell.reuseIdentifier) as? InvoiceCell else { return UITableViewCell() }
            
            return cell
        }
        else
        {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: InvoiceCell.reuseIdentifier) as? InvoiceCell else { return UITableViewCell() }
            
            return cell
        }
    }
    
    
}
