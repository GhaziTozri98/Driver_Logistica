//
//  HomeController.swift
//  Logistica_Driver_IOS
//
//  Created by Hassine Feker on 6/8/20.
//  Copyright © 2020 wimobi. All rights reserved.
//

import UIKit

class HomeController: MenuItemController
{
    @IBOutlet weak var tripsTableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setupData()
    }
    
    func setupData()
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1 ) {
            DriverTrips.getTripRequests { (success) in
                guard success else { return }
                DispatchQueue.main.async { [weak self] in
                    guard let self = self
                        else {return}
                    self.tripsTableView.reloadData()
                }
            }
        }
    }
    
    
    //MARK: - Actions
    
    @IBAction func notificationsButtonTapped(_ sender: Any)
    {
        let notificationsController = NotificationsController.instantiateFromStoryboard(HOME_STORYBORAD)
        navigationController?.pushViewController(notificationsController, animated: true)
    }
}

extension HomeController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        guard let tripsData = DriverTrips.authUserTrips.data else { return 0 }
        return tripsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TripCell.reuseIdentifier) as? TripCell,
            let tripsData = DriverTrips.authUserTrips.data,
            indexPath.item < tripsData.count
            else { return UITableViewCell() }
        
        cell.containerView.dropCardShadow()
        cell.setupUI(trip: tripsData[indexPath.item])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let tripDetailsController = TripDetailsController.instantiateFromStoryboard(HOME_STORYBORAD)
        navigationController?.pushViewController(tripDetailsController, animated: true)
    }
}
