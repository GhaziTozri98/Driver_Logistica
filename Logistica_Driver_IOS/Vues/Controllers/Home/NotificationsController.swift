//
//  NotificationsController.swift
//  Logistica_Driver_IOS
//
//  Created by Jewel Cheriaa on 4/28/20.
//  Copyright © 2020 Jewel Cheriaa. All rights reserved.
//

import UIKit

class NotificationsController: BaseController
{
    @IBOutlet weak var notificationsTableView: UITableView!
    
    private let refreshControl = UIRefreshControl()
    
    // MARK: - LIFE CYCLE
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setupUI()
        
        setupData()
    }
    
}



// MARK: - METHODS
extension NotificationsController
{
    func setRefreshControl()
    {
        refreshControl.tintColor = UIColor(red:0.25, green:0.72, blue:0.85, alpha: 1.0)
        notificationsTableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(fetchNotifications), for: .valueChanged)
    }
    
    func setupUI()
    {
        setRefreshControl()
        fetchNotifications()
    }
    
    func setupData()
    {
//        NotificationService.getNotifications { (success) in
//            guard success else { return }
//            DispatchQueue.main.async {[weak self] in
//                guard let self = self else { return }
//                self.notificationsTableView.reloadData()
//
//            }
//        }
    }
}



// MARK: - TABLE VIEW DELEGATE & DATASOURCE

extension NotificationsController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 4
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        return tableView.dequeueReusableCell(withIdentifier: NotificationCell.reuseIdentifier, for: indexPath) as!NotificationCell
    }

}

//extension NotificationsController: UITableViewDelegate, UITableViewDataSource
//{
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
//    {
//        return NotificationService.myNotifications.count
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
//    {
//        
//        guard
//            indexPath.row < NotificationService.myNotifications.count,
//            let invitationCell = tableView.dequeueReusableCell(withIdentifier: NotificationCell.reuseIdentifier, for: indexPath) as? NotificationCell else
//        { return UITableViewCell() }
//        
//        let notificationItem = NotificationService.myNotifications[indexPath.item]
//        invitationCell.notificationTitleLabel.text = notificationItem.title
//        invitationCell.notificationDescriptionLabel.text = notificationItem.description
//        
//        if let notificationIcon = notificationItem.icon
//        {
//            invitationCell.notificationImageView.activateSdWebImageLoader()
//            invitationCell.notificationImageView.sd_setImage(with: URL(string: notificationIcon), placeholderImage: UIImage(named: "profilePlaceholder"))
//        }
//        else
//        {
//            invitationCell.notificationImageView.image = UIImage(named: PlaceholdingImages.blackProfile.rawValue)
//        }
//        
//        return invitationCell
//    }
//    
//}


extension NotificationsController
{
    @objc func fetchNotifications()
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
            self?.refreshControl.endRefreshing()
        }
    }
}
