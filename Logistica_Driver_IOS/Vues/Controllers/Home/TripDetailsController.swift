//
//  TripDetailsController.swift
//  Logistica_Driver_IOS
//
//  Created by Jewel Cheriaa on 6/9/20.
//  Copyright © 2020 wimobi. All rights reserved.
//

import UIKit

class TripDetailsController: BaseController
{

    @IBOutlet weak var customerImageView: UIImageView!
    @IBOutlet weak var customerNameLabel: UILabel!
    @IBOutlet weak var tripDateLabel: UILabel!
    @IBOutlet weak var servicesTableView: ContentSizedTableView!
    @IBOutlet weak var attachementsCollectionView: UICollectionView!
    @IBOutlet weak var roomNumberStackView: UIStackView!
    @IBOutlet weak var roomNumberLabel: UILabel!
    @IBOutlet weak var attachementsStackView: UIStackView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        servicesTableView.contentInset = .init(top: 30, left: 0, bottom: 0, right: 0)
        attachementsCollectionView.contentInset = .init(top: 0, left: 25, bottom: 0, right: 25)
    }
    
}



extension TripDetailsController: UITableViewDataSource
{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ServiceCell.reuseIdentifier, for: indexPath) as? ServiceCell else { return UITableViewCell() }
        
        return cell
    }
}




extension TripDetailsController: UICollectionViewDelegate, UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AttachmentCell.reuseIdentifier, for: indexPath) as? AttachmentCell else { return UICollectionViewCell() }
        
        return cell
    }
    
}



class ServiceCell: UITableViewCell
{
    
}

class AttachmentCell: UICollectionViewCell
{
    @IBOutlet weak var attachmentImageView: UIImageView!
}
