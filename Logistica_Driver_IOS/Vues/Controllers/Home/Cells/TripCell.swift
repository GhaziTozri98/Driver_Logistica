//
//  TripCell.swift
//  Logistica_Driver_IOS
//
//  Created by Hassine Feker on 2020-06-10.
//  Copyright © 2020 wimobi. All rights reserved.
//

import UIKit

class TripCell: UITableViewCell
{
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var startPointPrimaryName: UILabel!
    @IBOutlet weak var startPointSecName: UILabel!
    @IBOutlet weak var endPointPrimaryName: UILabel!
    @IBOutlet weak var endPointSecName: UILabel!
    @IBOutlet weak var driverName: UILabel!
    @IBOutlet weak var driverImage: UIImageView!
    @IBOutlet weak var tripPrice: UILabel!
    @IBOutlet weak var pickupDate: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    
    func setupUI(trip: TripModel)
      {
        guard let pickupAddress = trip.addresses?.first(where: { (address) -> Bool in
            return address.type == "1"
        }) else {return}
        self.startPointPrimaryName.text = pickupAddress.primaryName
        self.startPointSecName.text = pickupAddress.secondaryName

        guard let destinationAddress = trip.addresses?.first(where: { (address) -> Bool in
            return address.type == "2"
        }) else {return}
        self.endPointPrimaryName.text = destinationAddress.primaryName
        self.endPointSecName.text = destinationAddress.secondaryName
        
        self.tripPrice.text = String(trip.totalPrice ?? 0) + "SAR".localizedString
        
        guard let driver = trip.user  else {return}
        if let driverPhoto = driver.imageURL
        {
            self.driverImage.activateSdWebImageLoader()
            self.driverImage.sd_setImage(with: URL(string: driverPhoto), placeholderImage: UIImage(named: "profilePlaceholder"))
        }
        else
        {
            driverImage.image = UIImage(named: PlaceholdingImages.profile.rawValue)
        }
        
        guard
            let firstName = driver.firstName,
            let lastName = driver.lastName
        else
        {
            return
        }
        self.driverName.text = firstName + " " + lastName
      }

}

  
extension UIView
{
    func dropCardShadow()
    {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.3
        self.layer.shadowOffset = CGSize(width: 0, height: 1.5)
        self.layer.shadowRadius = 1.3
    }
}
