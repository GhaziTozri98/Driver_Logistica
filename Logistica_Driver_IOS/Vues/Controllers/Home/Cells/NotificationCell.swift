//
//  NotificationCell.swift
//  Logistica_Driver_IOS
//
//  Created by Jewel Cheriaa on 4/28/20.
//  Copyright © 2020 Jewel Cheriaa. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell
{
    
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var notificationImageView: UIImageView!
    @IBOutlet weak var notificationTitleLabel: UILabel!
    @IBOutlet weak var notificationDescriptionLabel: UILabel!
    @IBOutlet weak var notificationDateLabel: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        imageContainerView.layer.cornerRadius = imageContainerView.frame.width / 2
        selectionStyle = .none
    }

//    func configureWithNotification(_ notification: NotificationModel)
//    {
//        let TitleAndColor = notificationTitleAndColorByType(notification.type)
//        notificationTitleLabel.text = TitleAndColor.title
//        notificationTitleLabel.textColor = TitleAndColor.color
//        notificationDescriptionLabel.text = notification.content
//        notificationDateLabel.text = notification.date
//        notificationImageView.image = UIImage(named: notification.type)
//    }
//
//    func notificationTitleAndColorByType(_ type: String) -> (title: String, color: UIColor)
//    {
//        var title = ""
//        var color = UIColor.lightGray
//        switch type
//        {
//        case "newDrinvingRequest":
//            title = NotificationTitle.newDrinvingRequest.rawValue
////            color = .cyan
//        case "acceptedDrinvingRequest":
//            title = NotificationTitle.acceptedDrinvingRequest.rawValue
////            color = .blue
//        case "confirmedDrinvingRequest":
//            title = NotificationTitle.confirmedDrinvingRequest.rawValue
////            color = .magenta
//        case "driverArrived":
//            title = NotificationTitle.driverArrived.rawValue
////            color = .brown
//        case "canceledDrinvingRequest":
//            title = NotificationTitle.canceledDrinvingRequest.rawValue
//            color = .red
//        case "finishedDrinvingRequest":
//            title = NotificationTitle.finishedDrinvingRequest.rawValue
//            color = .green
//        case "rejectedDrinvingRequest":
//            title = NotificationTitle.rejectedDrinvingRequest.rawValue
//            color = .brown
//        default:
//            return (title,color)
//        }
//        return (title,color)
//    }
}





