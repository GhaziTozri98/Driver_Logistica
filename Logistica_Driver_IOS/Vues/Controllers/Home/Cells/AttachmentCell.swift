//
//  AttachmentCell.swift
//  Logistica_Driver_IOS
//
//  Created by Hassine Feker on 2020-06-11.
//  Copyright © 2020 wimobi. All rights reserved.
//

import UIKit

class AttachmentCell: UICollectionViewCell
{
    @IBOutlet weak var attachmentImageView: UIImageView!
}
