//
//  LanguagesController.swift
//  Logistica_Driver_IOS
//
//  Created by Wimobi on 6/8/20.
//  Copyright © 2020 wimobi. All rights reserved.
//



import UIKit


protocol LanguageSelectionProtocol: AnyObject
{
    func didSelectLanguage(language: SupportedLanguages)
}


class LanguagesController: UIViewController
{
    @IBOutlet weak var englishImageView: UIImageView!
    @IBOutlet weak var arabicImageView: UIImageView!
    
    weak var delegate: LanguageSelectionProtocol?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setupViews()
    }
    
    //MARK: - METHODS
    
    func setupViews()
    {
        englishImageView.image = USING_ENGLISH == true ? UIImage(named: "check-icon") : nil
        arabicImageView.image = USING_ENGLISH == false ? UIImage(named: "check-icon") : nil
    }
    
    @IBAction func englishButtonTapped(_ sender: Any)
    {
        guard !USING_ENGLISH else { return }
        englishImageView.image = UIImage(named: "check-icon")
        arabicImageView.image = nil
        dismiss(animated: true) { [weak self] in
            self?.delegate?.didSelectLanguage(language: .english)
        }
    }
    
    
    @IBAction func arabicButtonTapped(_ sender: Any)
    {
        guard USING_ENGLISH else { return }
        englishImageView.image = nil
        arabicImageView.image = UIImage(named: "check-icon")
        dismiss(animated: true) { [weak self] in
            self?.delegate?.didSelectLanguage(language: .arabic)
        }
    }
}


