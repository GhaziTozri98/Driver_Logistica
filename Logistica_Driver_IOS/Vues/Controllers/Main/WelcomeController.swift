//
//  WelcomeController.swift
//  Logistica_Driver_IOS
//
//  Created by Wimobi on 6/5/20.
//  Copyright © 2020 wimobi. All rights reserved.
//

import UIKit

class WelcomeController: UIViewController
{
    @IBOutlet weak var langButton: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    // MARK:- IBACTIONS
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.langButton.setTitle(USING_ENGLISH == true ? "English" : "العربية" , for: .normal)
    }
    @IBAction func selectLanguageAction(_ sender: Any)
    {
        let selectLanguage = LanguagesController.instantiateFromStoryboard(LOGIN_STORYBORAD)
        selectLanguage.delegate = self
        present(selectLanguage, animated: true, completion: nil)
    }
    
    @IBAction func inputNumberAction(_ sender: Any)
    {
        let loginController = LoginController.instantiateFromStoryboard(LOGIN_STORYBORAD)
        self.navigationController?.pushViewController(loginController, animated: true)
    }
    
}

// MARK:- LanguageSelectionProtocol

extension WelcomeController: LanguageSelectionProtocol
{
    func didSelectLanguage(language: SupportedLanguages)
    {
        guard
            let appDelegate = UIApplication.shared.delegate as? AppDelegate,
            let keyWindow = appDelegate.window else
        { return }
        
        LanguageManager.setupUIKitComponentsSemantic(language: language.rawValue)
        
        let getStartedVC = WelcomeController.instantiateFromStoryboard(LOGIN_STORYBORAD)
        
        let navigationController = UINavigationController(rootViewController: getStartedVC)
        navigationController.isNavigationBarHidden = true
        keyWindow.rootViewController = navigationController
        
        
        keyWindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: keyWindow, duration: 0.5, options: USING_ENGLISH ? .transitionFlipFromRight : .transitionFlipFromLeft, animations: nil, completion: nil)
    }
}


enum SupportedLanguages: String
{
    case arabic = "ar"
    case english = "en"
}
