//
//  EnterYourNameController.swift
//  Logistica_Driver_IOS
//
//  Created by Ghazi Tozri on 6/12/20.
//  Copyright © 2020 wimobi. All rights reserved.
//

import UIKit
import AVFoundation
import DropDown

class EnterYourNameController: BaseController
{
    var destination = 0 // from 0 to 6, -1 for profile Picture
    var dropDown = DropDown()
    var isChecked = false
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var typeOfCarTextField: UITextField!
    @IBOutlet weak var agreeBox: UIButton!
    @IBOutlet var photosButtons: [UIButton]!
    @IBOutlet weak var termsLabel: UILabel!
    @IBOutlet weak var typeOfCarContainerView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupData()
        
    }
    func setupData()
    {
        UserService.getListCarsType { (success) in
            guard success else { return }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.configureDropDown()
            }
        }
    }
    
    func setupUI()
    {
        let homeLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
        
        let text = NSMutableAttributedString(string: "TermsAndConditions".localizedString)
        
        let attrs = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.patternDash.rawValue | NSUnderlineStyle.single.rawValue]
        
        text.addAttributes(attrs, range: NSRange(location: 0, length: text.length))
        
        termsLabel.attributedText = text
    }
    fileprivate func configureDropDown()
    {
        dropDown.anchorView = typeOfCarContainerView
        DropDown.appearance().textFont = UIFont(name: "Axiforma-R", size: 14)  ?? UIFont.systemFont(ofSize: 13)
        for car in UserService.carTypeList
        {
            dropDown.dataSource.append(USING_ENGLISH ? car.model.en : car.model.ar )
        }
    }
    
    @IBAction func agreeChecked(_ sender: Any)
    {
        let image = agreeBox.currentImage == UIImage(named: "checkedbox") ? UIImage(named: "checkbox_square")
            : UIImage(named: "checkedbox")
        agreeBox.setImage(image, for: .normal)
        isChecked = true
    }
    @IBAction func selectCarType(_ sender: Any)
    {
        DropDown.startListeningToKeyboard()
        dropDown.show()
        dropDown.selectionAction = { (index: Int, item: String) in
            DispatchQueue.main.async { [weak self] in
                guard
                    let self = self,
                    index < UserService.carTypeList.count else
                { return }
                self.typeOfCarTextField.text = item
                NewUser.parameters.carType = UserService.carTypeList[index].id ?? -1
            }
        }
    }
    @IBAction func openPhotoPickingSelector(_ sender: UIButton)
    {
        let destination = sender.tag
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized
        {
            preparePickerController(destination: destination)
        }
        else
        {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { [weak self] granted in
                guard granted else
                {
                    //                           self?.showDeniedCameraAccessAlert()
                    return
                }
                self?.preparePickerController(destination: destination)
            })
        }
    }
    @IBAction func goToHome(_ sender: Any)
    {
        if nameTextField.text == "" || lastNameTextField.text == "" || emailTextField.text == ""
        {
            GRP.showToast(failure: true, message: "CompleteRequiredFields".localizedString)
        }
        else
        {
            if !isChecked
            {
                GRP.showToast(failure: true, message: "YouMustAgree".localizedString)
            }
            else
            {
                guard
                    let firstName = self.nameTextField.text,
                    let lastName = self.lastNameTextField.text,
                    let email = self.emailTextField.text
                    else { return }
                NewUser.parameters.firstName = firstName
                NewUser.parameters.lastName = lastName
                NewUser.parameters.email = email
                //PS : Phone number is attributed in LoginController
                UserService.register { (success) in
                    guard success else { return }
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else { return }
                        let homeController = HomeController.instantiateFromStoryboard(HOME_STORYBORAD)
                        self.navigationController?.setViewControllers([homeController], animated: true)
                    }
                }
            }
        }
    }
    func preparePickerController(destination: Int)
    {
        DispatchQueue.main.async { [weak self] in
            self?.destination = destination
            let pickerSourceController = PickerSourceController.instantiateFromStoryboard(SETTINGS_STORYBORAD)
            pickerSourceController.delegate = self
            self?.present(pickerSourceController, animated: true, completion: nil)
        }
    }
}

// MARK: -Helper

extension EnterYourNameController: PhotoChoosingDelegate
{
    func openPicker(sourceType: UIImagePickerController.SourceType)
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = sourceType
            self?.present(imagePicker, animated: true, completion: nil)
        }
    }
}

//MARK: - UIImagePickerControllerDelegate

extension EnterYourNameController: UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        guard
            let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage,
            let jpegData = pickedImage.jpegData(compressionQuality: 0.01),
            let compressedImage = UIImage(data: jpegData) else
        {
            picker.dismiss(animated: true,completion: nil)
            return
        }
        
        picker.dismiss(animated: true) { [weak self] in
            guard let self = self else { return }
            
            guard self.destination >= 0 , self.destination <= 6 else { return }
            switch self.destination
            {
            case 0:
                NewUser.parameters.identity1 = jpegData
                break
            case 1:
                NewUser.parameters.identity2 = jpegData
                break
            case 2:
                NewUser.parameters.car1 = jpegData
                break
            case 3:
                NewUser.parameters.car2 = jpegData
                break
            case 4:
                NewUser.parameters.car3 = jpegData
                break
            case 5:
                NewUser.parameters.licence1 = jpegData
                break
            case 6:
                NewUser.parameters.licence2 = jpegData
                break
            default:
                break
            }
            self.photosButtons[self.destination].setImage(compressedImage, for: .normal)
            self.photosButtons[self.destination].isUserInteractionEnabled = false
            
            //self.deletePhotoButtons[self.destination-1].isHidden.toggle()
        }
    }
}

