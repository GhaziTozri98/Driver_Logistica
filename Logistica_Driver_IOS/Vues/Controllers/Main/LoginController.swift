//
//  LoginController.swift
//  Logisitica_iOS
//
//  Created by Jewel Cheriaa on 4/27/20.
//  Copyright © 2020 Jewel Cheriaa. All rights reserved.
//

import UIKit
import FlagPhoneNumber

class LoginController: BaseController
{
    
    
    @IBOutlet weak var phoneTextField: FPNTextField!
    @IBOutlet weak var continueButton: UIButton!
    
    let listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    
    
    
    //MARK: - LIFE CYCLE
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        phoneTextField.delegate = self
        phoneTextField.flagButtonSize = CGSize(width: 70, height: 26)
        phoneTextField.flagButton.imageEdgeInsets = USING_ENGLISH
            ? UIEdgeInsets(top: 0, left: -13, bottom: 0, right: 13)
            : UIEdgeInsets(top: 0, left: 13, bottom: 0, right: -13)
        phoneTextField.hasPhoneNumberExample = false
        
        phoneTextField.displayMode = .list
        listController.setup(repository: phoneTextField.countryRepository)
        listController.didSelect = { [weak self] country in
            guard let self = self else { return }
            self.phoneTextField.setFlag(countryCode: country.code)
        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
    }
    
    
    
    
    //MARK: - IBACTIONS
    
    @IBAction func goToCodeVerificationVC(_ sender: Any)
    {
        guard
            let phoneNumber = phoneTextField.getFormattedPhoneNumber(format: .E164),
            !phoneNumber.isEmpty() else
        { return }
        NewUser.parameters.phoneNumber = phoneNumber
        let codeVerificationController = CodeVerificationController.instantiateFromStoryboard(LOGIN_STORYBORAD)
        
        UserService.verifyPhone (phone: phoneNumber) { (success) in
            guard success else { return }
            
            DispatchQueue.main.async { [weak self] in
                guard  let self = self else { return }
                codeVerificationController.phoneNumber = phoneNumber
                self.navigationController?.pushViewController(codeVerificationController, animated: true)
            }
        }
    }
    
}



//MARK: - FPNTextFieldDelegate


extension LoginController: FPNTextFieldDelegate
{
    func fpnDisplayCountryList()
    {
        let navigationViewController = UINavigationController(rootViewController: listController)
        listController.title = "Countries".localizedString
        listController.searchController.searchBar.placeholder = "Search".localizedString
//        listController.searchController.clear 
        self.present(navigationViewController, animated: true, completion: nil)
    }
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String)
    {
        
    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool)
    {
        continueButton.isEnabled = isValid//isValid
        continueButton.alpha = isValid ? 1 : 0.5
    }
    
}

