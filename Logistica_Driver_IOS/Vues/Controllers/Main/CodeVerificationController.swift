//
//  CodeVerificationController.swift
//  Logistica_Driver_IOS
//
//  Created by Jewel Cheriaa on 4/27/20.
//  Copyright © 2020 Jewel Cheriaa. All rights reserved.
//

import UIKit
import KKPinCodeTextField


class CodeVerificationController: BaseController
{
    
    @IBOutlet weak var currentNumberLabel: UILabel!
    
    var phoneNumber = ""
    
    
    //MARK: - LIFE CYCLE
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    
    
    //MARK: - IBACTIONS
    
    @IBAction func codeTextFieldTextChanged(_ textField: KKPinCodeTextField)
    {
        guard
            let verificationCode = textField.text,
            verificationCode.count == 4 else
        { return }
        
        textField.resignFirstResponder()
        
        UserService.verifyCode(phone: phoneNumber, code: verificationCode) { (success) in
            guard success else { return }
            
            DispatchQueue.main.async { [weak self] in
                guard
                    let self = self,
                    let isAlreadyUser = UserService.authenticatedUser.isAlreadyUser else
                { return }
                if isAlreadyUser
                {
                    self.navigationController?.pushViewController(HomeController.instantiateFromStoryboard(HOME_STORYBORAD), animated: true)
                }
                else
                {
                    self.navigationController?.pushViewController(EnterYourNameController.instantiateFromStoryboard(LOGIN_STORYBORAD), animated: true)
                }
            }
        }
    }
    
    @IBAction func editCurrentNumber(_ sender: Any)
    {
    }
    
    func goToEnterName()
    {
        if currentNumberLabel.text != ""
        {
            let enterYourName = EnterYourNameController.instantiateFromStoryboard(LOGIN_STORYBORAD)
            self.navigationController?.pushViewController(enterYourName, animated: true)
        }
    }
}


//MARK: - LIFE CYCLE

extension CodeVerificationController
{
    func setupUI()
    {
        currentNumberLabel.text = phoneNumber
    }
}
