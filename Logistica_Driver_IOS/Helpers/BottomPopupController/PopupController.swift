//
//  PopupController.swift
//  ByArabs
//
//  Created by Mohamed Ali BELHADJ on 4/2/20.
//  Copyright © 2020 Creativity Echo. All rights reserved.
//

import UIKit

class PopupController: BottomPopupViewController
{
        
    override var popupTopCornerRadius: CGFloat { return 0 }
    
    override var popupPresentDuration: Double { return 0.4 }
    
    override var popupDismissDuration: Double { return  0.2 }
    
    override var popupShouldDismissInteractivelty: Bool { return true }
    
    override var popupDimmingViewAlpha: CGFloat { return 0.2 }
    
    
    @IBAction func closePopup(_ sender: Any?)
    {
        dismiss(animated: true, completion: nil)
    }
}
