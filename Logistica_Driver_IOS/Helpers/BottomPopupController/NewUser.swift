//
//  NewUser.swift
//  Logistica_Driver_IOS
//
//  Created by Ghazi Tozri on 6/17/20.
//  Copyright © 2020 wimobi. All rights reserved.
//

import Foundation

struct NewUser /// RegistrationController User
{
    static var parameters = RegistrationParameters()
    
    static func getCurrentState()
    {
        print("=====================================================")
        print("email            ", parameters.email)
        print("firstName            ", parameters.firstName)
        print("lastName            ", parameters.lastName)
        print("carType            ", parameters.carType)
        print("phoneNumber            ", parameters.phoneNumber)
        print("identity1         ", parameters.identity1 != nil)
        print("identity2         ", parameters.identity2 != nil)
        print("car1          ", parameters.car1 != nil)
        print("car2          ", parameters.car2 != nil)
        print("car3          ", parameters.car3 != nil)
        print("car3          ", parameters.licence1 != nil)
        print("car3          ", parameters.licence2 != nil)
        
        
    }
    
    struct RegistrationParameters
    {
        var email: String = ""
        var firstName: String = ""
        var lastName: String = ""
        var carType: Int = -1
        var phoneNumber: String = ""
        var identity1 : Data? = nil
        var identity2 : Data? = nil
        var car1 : Data? = nil
        var car2 : Data? = nil
        var car3 : Data? = nil
        var licence1 : Data? = nil
        var licence2 : Data? = nil
        var photo: Data? = nil
        var balance: Float? = 0
    }
    
    static func reset()
    {
        parameters = RegistrationParameters()
    }
}
