//
//  UIViewControllerWithKeyboard.swift
//  Logistic
//
//  Created by Wimobi on 5/26/20.
//  Copyright © 2020 wimobi. All rights reserved.
//

import UIKit

class UIViewControllerWithKeyboard: UIViewController
{
    deinit
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBOutlet weak var bottomConstant: NSLayoutConstraint!
    
    
    var defaultHeight: CGFloat = 20
    


    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShowNotification(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHideNotification(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    
    
    @objc func keyboardWillShowNotification(notification: Notification)
    {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            bottomConstant.constant = defaultHeight + keyboardHeight
        }
    }
    
    @objc func keyboardWillHideNotification(notification: Notification)
    {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            bottomConstant.constant -= keyboardHeight
        }
    }
}
