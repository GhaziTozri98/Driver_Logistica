//
//  LanguageManager.swift
//  UIKit+Combine
//
//  Created by Jewel Cheriaa on 6/1/20.
//  Copyright © 2020 Jewel Cheriaa. All rights reserved.
//


import UIKit
import FlagPhoneNumber
import KKPinCodeTextField
//MARK: - Language Setters

var USING_ENGLISH = true



//MARK: - LOCALIZATION SYSTEM => BUNDLE UPDATER

class LanguageManager: NSObject
{
    class func toggleLanguage()
    {
        MethodSwizzleGivenClassName(cls: Bundle.self, originalSelector: #selector(Bundle.localizedString(forKey:value:table:)), overrideSelector:
            #selector(Bundle.specialLocalizedString(key:value:table:)))
    }
    
    class func setupLanguageRequirements()
    {
        let currentLanguage = UserDefaults.standard.value(forKey: "AppleLanguage") as? String != nil
            ? UserDefaults.standard.value(forKey: "AppleLanguage") as? String
            : Locale.current.languageCode
        
        setupUIKitComponentsSemantic(language: currentLanguage ?? "en")
        
        toggleLanguage()
    }
    
    static func setupUIKitComponentsSemantic(language: String)
    {
        UserDefaults.standard.set(language, forKey: "AppleLanguage")
        USING_ENGLISH = language == "en"
        let semantic: UISemanticContentAttribute = USING_ENGLISH ? .forceLeftToRight : .forceRightToLeft
        UIView.appearance().semanticContentAttribute = semantic
        UITabBar.appearance().semanticContentAttribute =  semantic
        UINavigationBar.appearance().semanticContentAttribute = semantic
        UIScrollView.appearance().semanticContentAttribute = semantic
        UICollectionView.appearance().semanticContentAttribute = semantic
        UITableView.appearance().semanticContentAttribute = semantic
        FPNTextField.appearance().semanticContentAttribute = semantic
        KKPinCodeTextField.appearance().semanticContentAttribute = .spatial
    }
}



func MethodSwizzleGivenClassName(cls: AnyClass, originalSelector: Selector, overrideSelector: Selector)
{
    guard
        let origMethod: Method = class_getInstanceMethod(cls, originalSelector),
        let overrideMethod: Method = class_getInstanceMethod(cls, overrideSelector) else
    { return }
    
    guard (class_addMethod(cls, originalSelector, method_getImplementation(overrideMethod), method_getTypeEncoding(overrideMethod))) else
    {
        method_exchangeImplementations(origMethod, overrideMethod)
        return
    }
    
    class_replaceMethod(cls, overrideSelector, method_getImplementation(origMethod), method_getTypeEncoding(origMethod))
}


extension Bundle
{
    @objc func specialLocalizedString(key: String, value: String?, table tableName: String?) -> String?
    {
        var bundle = Bundle()
        if let _path = Bundle.main.path(forResource: UserDefaults.standard.value(forKey: "AppleLanguage") as? String ?? Locale.current.languageCode, ofType: "lproj"), let disiredBundle = Bundle(path: _path)
        {
            bundle = disiredBundle
        }
        else if let _path = Bundle.main.path(forResource: "Base", ofType: "lproj"), let disiredBundle = Bundle(path: _path)
        {
            bundle = disiredBundle
        }
        return (bundle.specialLocalizedString(key: key, value: value, table: tableName))
    }
}





//MARK: - INTERFACE BUILDER LOACALIZING HELPERS

protocol XIBLocalizable
{
    var locString: String? { get set }
}

extension UILabel: XIBLocalizable
{
    @IBInspectable var locString: String? {
        get { return nil }
        set(key) {
            text = key?.localizedString
        }
    }
}

extension UIButton: XIBLocalizable
{
    @IBInspectable var locString: String? {
        get { return nil }
        set (key)
        {
            setTitle(key?.localizedString, for: .normal)
        }
    }
}

extension UITextField: XIBLocalizable
{
    @IBInspectable var locString: String? {
        get { return nil }
        set (key)
        {
            placeholder = key?.localizedString
        }
    }
}






//MARK: - UIKIT COMPONENTS MODIFIERS

extension UITextField
{
    open override func awakeFromNib()
    {
        super.awakeFromNib()
        
        guard tag != -10 else { return }
        if textAlignment != .center, textAlignment != .justified
        {
            textAlignment =  USING_ENGLISH ? .left : .right
        }
    }
}

extension UILabel
{
    open override func awakeFromNib()
    {
        super.awakeFromNib()
        
        if textAlignment != .center, textAlignment != .justified
        {
            textAlignment =  USING_ENGLISH ? .left : .right
        }
    }
}

extension UITextView
{
    open override func awakeFromNib()
    {
        super.awakeFromNib()
        
        if textAlignment != .center, textAlignment != .justified
        {
            textAlignment =  USING_ENGLISH ? .left : .right
        }
    }
}

extension UIButton
{
    open override func awakeFromNib()
    {
        super.awakeFromNib()
        
        if contentHorizontalAlignment != .center
        {
            contentHorizontalAlignment = USING_ENGLISH ? .left : .right
        }
    }
}


//MARK:  - STRING

extension String
{
    var localizedString: String
    {
        return NSLocalizedString(self, comment: "")
    }
}




class BaseController: UIViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        navigationController?.view.semanticContentAttribute = USING_ENGLISH ? .forceLeftToRight : .forceRightToLeft
        navigationController?.navigationBar.semanticContentAttribute = USING_ENGLISH ? .forceLeftToRight : .forceRightToLeft
    }
    
    @IBAction func popViewController(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
    }
    
}
