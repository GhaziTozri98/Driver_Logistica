//
//  GenericRequestPerformer.swift
//
//  Created by JEWEL CHERIAA on 2020-05-25.
//

import UIKit


struct SuccessfulResponseModel <ClassType: Decodable>: Decodable
{
    var message: String?
    var success: Bool?
    var response: ClassType?
}


struct ErrorModel: Decodable
{
    var message: String?
    var success: Bool?
    var time: Double?
}

struct GRP
{
    static var baseApiUrl: String = ""
    static var preferedFontName: String = ""
    static var defaultLoaderType: NVActivityIndicatorType = .circleStrokeSpin
    static var successToastBaseColor: UIColor = #colorLiteral(red: 0.3396788239, green: 0.6912397146, blue: 0.3835897744, alpha: 1)
    static var failureToastBaseColor: UIColor = #colorLiteral(red: 0.9098039216, green: 0.5098039216, blue: 0.5176470588, alpha: 1)
    static var toastDuration: Double = 0.5
    static var isPresentingToast = false

    static func performRequest <CodableModel: Decodable>
        (withLogs logsEnabled: Bool = false,
         withLoader loaderEnabled: Bool = true, loaderMessage: String = "PleaseWait".localizedString,
         loaderType: NVActivityIndicatorType = defaultLoaderType,
         withSuccesToast: Bool = false, successToastMessage: String = "Success".localizedString,
         endPoint: String,
         method: Method = .post,
         body : [String: Any]? = nil,
         encoding: Encoding = .URLEncoding,
         headers: [String: String]? = nil,
         bearerToken : String? = nil,
         completion: @escaping (CodableModel?) -> () )
    {
            
            guard !baseApiUrl.isEmpty else
            {
                showToast(failure: true, message: ErrorMessages().MissingBaseApiUrl)
                completion(nil)
                return
            }
            
            showLoader(shouldShow: loaderEnabled, loaderMessage: loaderMessage, loaderType: loaderType)

            guard GRP().isConnectedToNetwork() else
            {
                hideLoader()
                showToast(failure: true, message: ErrorMessages().NoInternetConnexion)
                completion(nil)
                return
            }
            
            guard let Url = URL(string: baseApiUrl + endPoint) else
            {
                hideLoader()
                showToast(failure: true, message: ErrorMessages().InvalidApiUrl)
                completion(nil)
                return
            }
            
            GRP().showRequest(url: endPoint, method: method, encoding: encoding, body: body, headers: headers, bearerToken: bearerToken, withLogs: logsEnabled)
            
            
            let Session_Request = GRP().createSessionAndRequest(withLogs: logsEnabled, Url: Url, method: method, body: body, encoding: encoding, headers: headers, bearerToken: bearerToken)
            
            Session_Request.session.dataTask(with: Session_Request.request,
                                             completionHandler: { responseData, _ , error in
                guard error == nil, let data = responseData else
                {
                    hideLoader()
                    showToast(failure: true, message: ErrorMessages().MissingResponseData)
                    completion(nil)
                    return
                }
                
                if let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
                {
                    GRP().showResponse(json: json, withLogs: logsEnabled)
                }
                
                do
                {
                    if let errorResponse = try? JSONDecoder().decode(ErrorModel.self, from: data),
                    let success = errorResponse.success, !success, let errorMessage = errorResponse.message
                    {
                        hideLoader()
                        completion(nil)
                        showToast(failure: true, message: errorMessage)
                    }
                    else
                    {
                        let objectFromJson = try JSONDecoder().decode(CodableModel.self, from: data)
                        hideLoader()
                        completion(objectFromJson)
                        guard withSuccesToast else { return }
                        GRP.showToast(message: successToastMessage)
                    }
                }
                catch let error
                {
                    GRP().describeError(error: error, withLogs: logsEnabled)
                    
                    if let errorResponse = try? JSONDecoder().decode(ErrorModel.self, from: data),
                        let timeStamp = errorResponse.time, let errorMessage = errorResponse.message
                    {
                        guard
                            let expirationDate = UserService.authenticatedUser.expirationDate,
                            timeStamp > expirationDate else
                        {
                            hideLoader()
                            completion(nil)
                            showToast(failure: true, message: errorMessage)
                            return
                        }
                        UserService.refreshToken { success in
                            guard success else
                            {
                                hideLoader()
                                completion(nil)
                                DispatchQueue.main.async {
                                    mainNavigationController()?.setViewControllers([LoginController.instantiateFromStoryboard(LOGIN_STORYBORAD)], animated: true)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                          showToast(failure: true, message: ErrorMessages().TokenRefreshingFailed)
                                      }
                                }
                                return
                            }
                            performRequest(withLogs: logsEnabled,
                                           withLoader: false,
                                           loaderMessage: loaderMessage,
                                           loaderType: loaderType,
                                           withSuccesToast: withSuccesToast,
                                           successToastMessage: successToastMessage,
                                           endPoint: endPoint,
                                           method: method,
                                           body: body,
                                           encoding: encoding,
                                           headers: headers,
                                           bearerToken: bearerToken,
                                           completion: completion)
                        }
                    }
                    else
                    {
                        hideLoader()
                        completion(nil)
                        showToast(failure: true, message: ErrorMessages().DecodingError)
                    }
                }
            }).resume()
    }
    
        
    func createSessionAndRequest(
        withLogs logsEnabled: Bool = false,
        Url: URL,
        method: Method = .post,
        body : [String: Any]? = nil,
        encoding: Encoding = .URLEncoding,
        headers: [String: String]? = nil,
        bearerToken : String? = nil)
        -> (session: URLSession, request: URLRequest)
    {
        let sessionConfig = URLSessionConfiguration.default
        
        var request = URLRequest(url: Url)
        request.httpMethod = method.rawValue
        
        request.httpBody = prepareBody(body: body, encoding: encoding, withLogs: logsEnabled)
        
        if method == .post || method == .put
        {
            request.allHTTPHeaderFields = ["Content-Type": "application/json"]
        }
        else
        {
        request.allHTTPHeaderFields = ["Accept": "application/json"]
        }
        sessionConfig.httpAdditionalHeaders = ["X-localization": USING_ENGLISH ? "en" : "ar"]

        if let headers = headers
        {
            headers.forEach { (key, value) in
                sessionConfig.httpAdditionalHeaders?[key] = value
            }
        }
        
        if let bearerToken = bearerToken
        {
            sessionConfig.httpAdditionalHeaders?["Authorization"] = "Bearer \(bearerToken)"
        }
        

        return (URLSession(configuration: sessionConfig, delegate: nil , delegateQueue: nil), request)
    }
}




import NVActivityIndicatorView

extension GRP
{
    static func showLoader(shouldShow: Bool = true, loaderMessage: String, loaderType: NVActivityIndicatorType = .circleStrokeSpin)
    {
        guard shouldShow else { return }
        
        DispatchQueue.main.async {
            guard !NVActivityIndicatorPresenter.sharedInstance.isAnimating else { return }
            
            let activityData = ActivityData(message: loaderMessage.localizedString,
                                            messageFont: UIFont(name: preferedFontName, size: 19) ?? UIFont.boldSystemFont(ofSize: 19),
                                            type: loaderType,
                                            color: .white,
                                            minimumDisplayTime: 1)
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        }
    }
    
    static func hideLoader()
    {
        DispatchQueue.main.async {
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        }
    }
}



extension GRP
{
    func prepareBody(body: [String: Any]?, encoding: Encoding, withLogs: Bool) -> Data?
    {
        guard let secureBody = body else { return nil }
        var httpBody: Data?
        switch encoding
        {
        case .JSONEncoding:
            httpBody = try? JSONSerialization.data(withJSONObject: secureBody, options: [])
            httpBody != nil ? showJsonEncodedBody(bodyData: httpBody!, withLogs: withLogs) : ()
            return httpBody
            
        case .URLEncoding:
            var bodyUrlEncodedString = ""
            for (key, value) in secureBody
            {
                bodyUrlEncodedString += "\(key)=\(value)&"
            }
            bodyUrlEncodedString = String(bodyUrlEncodedString.dropLast())
            showUrlEncodedBody(bodyString: bodyUrlEncodedString, withLogs: withLogs)
            httpBody  = bodyUrlEncodedString.data(using: .utf8)
            return httpBody
        }
    }
}




extension GRP
{
    func showRequest(url:String, method: Method, encoding: Encoding, body : [String:Any]?, headers: [String:String]?, bearerToken : String? = nil,  withLogs: Bool)
    {
        guard withLogs else { return }
        print("--------------------------------------------------------------------------------------------------------")
        print("----  REQUEST   ----------------------------------------------------------------------------------------")
        print("----------------  ")
        print("            URL                ==>   ", url)
        print("            METHOD             ==>   ", method)
        print("            BODY               ==>   ", body ?? "nil")
        print("            BODY ENCODING      ==>   ", encoding)
        print("            HEADERS            ==>   ", headers ?? "nil")
        print("            BEARER TOKEN       ==>   ", bearerToken ?? "nil")
        print("-------------------------------------------------------------------------------------- END OF REQUEST --\n\n")
    }
    
    func showJsonEncodedBody(bodyData: Data, withLogs: Bool)
    {
        guard withLogs else { return }
        print("---------------------------------------------------------------------------------------------------------")
        print("----  BODY AFTER JSON ENCODING   ------------------------------------------------------------------------")
        print("-----------------------------------  ")
        print("    Data Size:    ",bodyData.description)
        print("--------------------------------------------------------------------------------------- END OF BODY    --\n\n")
    }
    
    func showUrlEncodedBody(bodyString: String, withLogs: Bool)
    {
        guard withLogs else { return }
        print("---------------------------------------------------------------------------------------------------------")
        print("----  BODY AFTER URL ENCODING    ------------------------------------------------------------------------")
        print("---------------------------------  ")
        print("            ",bodyString)
        print("--------------------------------------------------------------------------------------- END OF BODY    --\n\n")
    }
    
    func showResponse(json: Any, withLogs: Bool)
    {
        guard withLogs else { return }
        print("---------------------------------------------------------------------------------------------------------")
        print("----  RESPONSE   ----------------------------------------------------------------------------------------")
        print("-----------------  ")
        print(json)
        print("-------------------------------------------------------------------------------------- END OF RESPONSE --\n\n")
    }
    
    func describeError(error: Error, withLogs: Bool)
    {
        guard withLogs else { return }
        print("---------------------------------------------------------------------------------------------------------------")
        print("----  ERROR   -------------------------------------------------------------------------------------------------")
        print("-----------------  ")
        print("    Description ==> ",error.localizedDescription, "\n")
        print("    Solution    ==>  Compare your Decodable Model to the json response OR Consider Changing the request Encoding.")
        print("-------------------------------------------------------------------------------------------- END OF ERROR    --\n\n")
    }
}




import SystemConfiguration

extension GRP
{
    func isConnectedToNetwork() -> Bool
    {
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false
        {
            return false
        }
        
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
}




extension GRP
{
    static func showToast(failure: Bool = false, message: String, droppedUp: Bool = false)
    {
        guard
            !message.isEmpty,
            !isPresentingToast else
        { return }
        
        DispatchQueue.main.async {
            
            guard let keyWindow: UIWindow = UIApplication.shared.delegate?.window ?? UIWindow() else { return }
            
            let localizedMessage = message.localizedString
            let button = UIButton(frame: CGRect.zero)
            button.setImage(UIImage(named: failure ? "checkmark" : "checkmark"), for: .normal)
            button.imageView?.tintColor = failure ? failureToastBaseColor : successToastBaseColor
            button.contentHorizontalAlignment = .center
            button.titleLabel?.textAlignment = .center
            button.setTitle(localizedMessage, for: .normal)
            button.titleLabel?.font = UIFont(name: preferedFontName, size: 16) ?? UIFont.boldSystemFont(ofSize: 16)
            button.layer.borderColor = failure ? failureToastBaseColor.cgColor : successToastBaseColor.cgColor
            button.layer.borderWidth =  0.5
            button.backgroundColor = .white
            button.setTitleColor(failure ? failureToastBaseColor : successToastBaseColor, for: .normal)
            button.titleLabel?.numberOfLines = 4
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 5
            let attributedString = NSMutableAttributedString(string: localizedMessage)
            attributedString.addAttribute(.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
            button.setAttributedTitle(attributedString, for: .normal)
            button.clipsToBounds = true
            button.layer.cornerRadius = 10
            button.isUserInteractionEnabled = false
            button.imageEdgeInsets = USING_ENGLISH ? .init(top: 0, left: -8, bottom: 0, right: 8) : .init(top: 0, left: 8, bottom: 0, right: -8)
            button.titleEdgeInsets = .init(top: 12, left: 12, bottom: 12, right: 12)
            var buttonWidth = GRP().width(text: localizedMessage, withConstrainedHeight: 15, font: UIFont(name: preferedFontName, size: 16) ?? UIFont.boldSystemFont(ofSize: 16)) + 50
            if buttonWidth > keyWindow.frame.width - 100
            {
                buttonWidth = keyWindow.frame.width - 100
            }
            let buttonheight = GRP().height(text: localizedMessage, withConstrainedWidth: keyWindow.frame.size.width - 100, font: UIFont(name: preferedFontName, size: 16) ?? UIFont.boldSystemFont(ofSize: 16)) + 25
            let abscisse = (keyWindow.frame.width/2) - (buttonWidth/2)
            button.frame = CGRect.init(x: abscisse, y: keyWindow.frame.maxY, width: buttonWidth, height: buttonheight )
            keyWindow.endEditing(true)
            keyWindow.addSubview(button)
            
            var basketTopFrame: CGRect = button.frame
            basketTopFrame.origin.x = abscisse
            basketTopFrame.origin.y = keyWindow.frame.maxY - (droppedUp ? 170 : 70) - button.frame.height
            
            UIView.animate(withDuration: 1, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.1, options: .curveEaseOut, animations: {
                button.frame = basketTopFrame
                isPresentingToast = true
            },completion: { _ in
                DispatchQueue.main.asyncAfter(deadline: .now() + (failure ? toastDuration : toastDuration)) {
                    UIView.animate(withDuration: 1.5, delay: 0.5, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.1, options: .curveEaseIn, animations: {
                        button.alpha = 0
                    },completion: { _ in
                        isPresentingToast = false
                        button.removeFromSuperview()
                    })
                }
            })
        }
    }
    
    func height(text: String, withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat
    {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = text.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(text: String, withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat
    {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = text.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}



extension GRP
{
    enum Method: String
    {
        case options = "OPTIONS"
        case get     = "GET"
        case head    = "HEAD"
        case post    = "POST"
        case put     = "PUT"
        case patch   = "PATCH"
        case delete  = "DELETE"
        case trace   = "TRACE"
        case connect = "CONNECT"
    }
    
    enum Encoding: String
    {
        case JSONEncoding
        case URLEncoding
    }
    
    struct ErrorMessages
    {
        let NoInternetConnexion       = "NoInternetConnexion".localizedString
        let InvalidApiUrl             = "InvalidApiUrl".localizedString
        let MissingResponseData       = "MissingResponseData".localizedString
        let DecodingError             = "DecodingError".localizedString
        let MissingBaseApiUrl         = "MissingBaseApiUrl".localizedString
        let TokenRefreshingFailed     = "TokenRefreshingFailed".localizedString
    }
}


