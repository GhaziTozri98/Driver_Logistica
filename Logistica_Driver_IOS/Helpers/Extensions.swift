
import UIKit
import SDWebImage



open class GradientViewInitializer : UIView
{
    open override func awakeFromNib()
    {
        super.awakeFromNib()
        backgroundColor = .clear
    }
}
open class GradientToCenterView: GradientViewInitializer
{
    open override func awakeFromNib()
    {
        super.awakeFromNib()
        applyGradient(colours: [ .clear, #colorLiteral(red: 0.03921568627, green: 0.03529411765, blue: 0.03529411765, alpha: 0.2965806935) , .clear])
    }
}

open class GradientToBottomView: GradientViewInitializer
{
    open override func awakeFromNib()
    {
        super.awakeFromNib()
        applyGradient(colours: [.clear , #colorLiteral(red: 0.03921568627, green: 0.03529411765, blue: 0.03529411765, alpha: 0.2965806935) ])
    }
}

open class GradientToTopView: GradientViewInitializer
{
    open override func awakeFromNib()
    {
        super.awakeFromNib()
        applyGradient(colours: [ #colorLiteral(red: 0.03921568627, green: 0.03529411765, blue: 0.03529411765, alpha: 0.2965806935) , .clear])
    }
}


open class GradientToTopAndBottomView: GradientViewInitializer
{
    open override func awakeFromNib()
    {
        super.awakeFromNib()
        applyGradient(colours: [ #colorLiteral(red: 0.03921568627, green: 0.03529411765, blue: 0.03529411765, alpha: 0.2965806935) , .clear, #colorLiteral(red: 0.03921568627, green: 0.03529411765, blue: 0.03529411765, alpha: 0.2965806935) ])
    }
}

//MARK:  VIEW CONTROLLER
extension UIViewController
{
    class func instantiateFromStoryboard(_ name: String) -> Self
    {
        return instantiateFromStoryboardHelper(name)
    }
    
    fileprivate class func instantiateFromStoryboardHelper<T>(_ name: String) -> T
    {
        let storyboard = UIStoryboard(name: name, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! T
        return controller
    }
    
    var hasTopNotch: Bool
    {
        if #available(iOS 13.0,  *)
        {
            return UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.safeAreaInsets.top ?? 0 > 20
        }
        else
        {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
    }
    
    @IBAction func goBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension UIApplication
{
    class var topViewController: UIViewController?
    {
        return getTopViewController()
    }
    
    private class func getTopViewController() -> UIViewController?
    {
        guard
            let appDelegate = UIApplication.shared.delegate as? AppDelegate,
            let mainWindow = appDelegate.window,
            let rootViewController = mainWindow.rootViewController as? UINavigationController else
        { return nil }
        
        if let presentedViewController = rootViewController.presentedViewController
        {
            return presentedViewController
        }
        return rootViewController
    }
}

extension UIApplication
{
    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController?
    {
        if let nav = base as? UINavigationController
        {
            return getTopViewController(base: nav.visibleViewController)
        }
        else if let tab = base as? UITabBarController, let selected = tab.selectedViewController
        {
            return getTopViewController(base: selected)
        }
        else if let presented = base?.presentedViewController
        {
            return getTopViewController(base: presented)
        }
        return base
    }
}

extension UIView
{
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat)
    {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func applyGradient(colours: [UIColor]) -> Void
    {
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void
    {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
}

extension UIColor
{
    convenience init(_ r: Double,_ g: Double,_ b: Double,_ a: Double)
    {
        self.init(red: CGFloat(r/255), green: CGFloat(g/255), blue: CGFloat(b/255), alpha: CGFloat(a))
    }
}

extension UIColor
{
    convenience init(hexString: String)
    {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt64()
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}



//MARK:  TABLEVIEW, COLLECTIONVIEW & SCROLLVIEW

final class ContentSizedTableView: UITableView
{
    override var contentSize:CGSize
    {
        didSet
        {
            invalidateIntrinsicContentSize()
        }
    }

    override var intrinsicContentSize: CGSize
    {
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
}


public extension UICollectionViewCell
{
    static var reuseIdentifier: String
    {
        return String(describing: self)
    }
}

public extension UITableViewCell
{
    static var reuseIdentifier: String
    {
        return String(describing: self)
    }
}

public extension UICollectionView
{
    
    func registerCellClass(_ cellClass: AnyClass)
    {
        let identifier = String.className(cellClass)
        self.register(cellClass, forCellWithReuseIdentifier: identifier)
    }
    
    func registerCellNib(_ cellClass: AnyClass)
    {
        let identifier = String.className(cellClass)
        let nib = UINib(nibName: identifier, bundle: nil)
        self.register(nib, forCellWithReuseIdentifier: identifier)
    }
    func reloadData(_ completion: @escaping () -> Void)
    {
        reloadData()
        DispatchQueue.main.async { completion() }
    }
}

public extension UITableView
{
    func registerCellClass(_ cellClass: AnyClass)
    {
        let identifier = String.className(cellClass)
        self.register(cellClass, forCellReuseIdentifier: identifier)
    }
    
    func registerCellNib(_ cellClass: AnyClass)
    {
        let identifier = String.className(cellClass)
        let nib = UINib(nibName: identifier, bundle: nil)
        self.register(nib, forCellReuseIdentifier: identifier)
    }
    func reloadData(_ completion: @escaping () -> Void)
    {
        reloadData()
        DispatchQueue.main.async { completion() }
    }
}



extension UIScrollView
{
    func scrollToBottom(animated: Bool)
    {
        if self.contentSize.height < self.bounds.size.height { return }
        let bottomOffset = CGPoint(x: 0, y: self.contentSize.height - self.bounds.size.height)
        self.setContentOffset(bottomOffset, animated: animated)
    }
}

//MARK:  STRING

extension String
{
    static func className(_ aClass: AnyClass) -> String
    {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
}


//MARK:  UIVIEW
public extension UIView
{
    class func fromNib<T: UIView>() -> T
    {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}



extension UIView
{
    @IBInspectable
    var cornerRadius: CGFloat
    {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
}

extension UIView
{
    func addShadow(shadowColor: UIColor, offSet: CGSize, opacity: Float, shadowRadius: CGFloat, cornerRadius: CGFloat, corners: UIRectCorner, fillColor: UIColor = .white)
    {
        let shadowLayer = CAShapeLayer()
        let size = CGSize(width: cornerRadius, height: cornerRadius)
        let cgPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: size).cgPath
        shadowLayer.path = cgPath
        shadowLayer.fillColor = fillColor.cgColor
        shadowLayer.shadowColor = shadowColor.cgColor
        shadowLayer.shadowPath = cgPath
        shadowLayer.shadowOffset = offSet
        shadowLayer.shadowOpacity = opacity
        shadowLayer.shadowRadius = shadowRadius
        self.layer.addSublayer(shadowLayer)
    }
}

extension UITextField
{
    func shake(shakeColor: UIColor, initialColor: UIColor,  shakeText: String, initialText: String, reverseSecureEntry: Bool = false)
    {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
        textColor = #colorLiteral(red: 0.9764705882, green: 0.662745098, blue: 0.5882352941, alpha: 1) //shakeColor
        text = shakeText
        if reverseSecureEntry
        {
            isSecureTextEntry.toggle()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            if reverseSecureEntry
            {
                self?.isSecureTextEntry.toggle()
            }
            self?.textColor = initialColor
            self?.text = initialText
        }
    }
}



//MARK:  UITextField
extension UITextField
{
    @IBInspectable var placeHolderColor: UIColor?
        {
        get
        {
            return self.placeHolderColor
        }
        set
        {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}


@IBDesignable class PaddingLabel: UILabel
{
    @IBInspectable var topInset: CGFloat = 5.0
    @IBInspectable var bottomInset: CGFloat = 5.0
    @IBInspectable var leftInset: CGFloat = 7.0
    @IBInspectable var rightInset: CGFloat = 7.0
    
    override func drawText(in rect: CGRect)
    {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize
    {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset, height: size.height + topInset + bottomInset)
    }
}




extension UIImage
{
    var flipped: UIImage
    {
        guard let cgImage = cgImage else
        {
            return self
        }
        
        return UIImage(cgImage: cgImage, scale: scale, orientation: .upMirrored)
    }
}

//MARK:  UIBUTTON
typealias GradientPoints = (startPoint: CGPoint, endPoint: CGPoint)


enum GradientOrientation
{
    case topRightBottomLeft
    case topLeftBottomRight
    case horizontal
    case vertical
    
    var startPoint : CGPoint
    {
        return points.startPoint
    }
    
    var endPoint : CGPoint
    {
        return points.endPoint
    }
    
    var points : GradientPoints
    {
        switch self
        {
        case .topRightBottomLeft:
            return (CGPoint(x: 0.0,y: 1.0), CGPoint(x: 1.0,y: 0.0))
        case .topLeftBottomRight:
            return (CGPoint(x: 0.0,y: 0.0), CGPoint(x: 1,y: 1))
        case .horizontal:
            return (CGPoint(x: 0.0,y: 0.5), CGPoint(x: 1.0,y: 0.5))
        case .vertical:
            return (CGPoint(x: 0.0,y: 0.0), CGPoint(x: 0.0,y: 1.0))
        }
    }
    
    
}



class GradientButton: UIButton
{
    override class func awakeFromNib()
    {
        super.awakeFromNib()
    }
    
    override open class var layerClass: AnyClass
    {
        return CAGradientLayer.classForCoder()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        let gradientLayer = layer as! CAGradientLayer
        gradientLayer.frame = self.bounds
        let orientation = GradientOrientation.horizontal
        gradientLayer.startPoint = orientation.startPoint
        gradientLayer.endPoint = orientation.endPoint
        gradientLayer.colors = [#colorLiteral(red: 0.6823529412, green: 0.7803921569, blue: 0.9411764706, alpha: 1).cgColor, #colorLiteral(red: 0.8156862745, green: 0.8784313725, blue: 0.9803921569, alpha: 1).cgColor]
    }
}


extension String
{
    var containsNonEnglishNumbers: Bool
    {
        return !isEmpty && range(of: "[^0-9]", options: .regularExpression) == nil
    }
    
    var english: String
    {
        return self.applyingTransform(StringTransform.toLatin, reverse: false) ?? self
    }
    
    func isEmpty() -> Bool
    {
        return  self.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
}

func height(text: String, withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat
{
    let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
    let boundingBox = text.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
    
    return ceil(boundingBox.height)
}

func width(text: String, withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat
{
    let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
    let boundingBox = text.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
    
    return ceil(boundingBox.width)
}


extension UIAlertController
{
    class func alert(title:String, msg:String, target: UIViewController)
    {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
                (result: UIAlertAction) -> Void in
            })
            target.present(alert, animated: true, completion: nil)
        }
    }
}


extension NSMutableAttributedString
{
    func setColor(color: UIColor, forText stringValue: String)
    {
        let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
    }
    
    func setFont(font: UIFont, forText stringValue: String)
    {
        let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
        self.addAttribute(NSAttributedString.Key.font, value: font, range: range)
    }
}

 // TextField and TextView Max Length
  private var kAssociationKeyMaxLength: Int = 0
  private var kAssociationKeyMaxLengthTextView: Int = 0
  extension UITextField {


      @IBInspectable var maxLength: Int {
          get {
              if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                  return length
              } else {
                  return Int.max
              }
          }
          set {
              objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
              addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
          }
      }

      @objc func checkMaxLength(textField: UITextField) {
          guard let prospectiveText = self.text,
              prospectiveText.count > maxLength
              else {
                  return
          }

          let selection = selectedTextRange

          let indexEndOfText = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
          let substring = prospectiveText[..<indexEndOfText]
          text = String(substring)

          selectedTextRange = selection
      }
  }

extension UITextView:UITextViewDelegate {


    @IBInspectable var maxLength: Int {
        get {
            if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLengthTextView) as? Int {
                return length
            } else {
                return Int.max
            }
        }
        set {
            self.delegate = self

            objc_setAssociatedObject(self, &kAssociationKeyMaxLengthTextView, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }

    public func textViewDidChange(_ textView: UITextView) {
        checkMaxLength(textField: self)
    }
    @objc func checkMaxLength(textField: UITextView) {
        guard let prospectiveText = self.text,
            prospectiveText.count > maxLength
            else {
                return
        }

        let selection = selectedTextRange

        let indexEndOfText = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        let substring = prospectiveText[..<indexEndOfText]
        text = String(substring)

        selectedTextRange = selection
    }
}

// Add shadow to uiview
extension UIView{

    @IBInspectable var shadowOffset: CGSize{
        get{
            return self.layer.shadowOffset
        }
        set{
            self.layer.shadowOffset = newValue
        }
    }

    @IBInspectable var shadowColor: UIColor{
        get{
            return UIColor(cgColor: self.layer.shadowColor!)
        }
        set{
            self.layer.shadowColor = newValue.cgColor
        }
    }

    @IBInspectable var shadowRadius: CGFloat{
        get{
            return self.layer.shadowRadius
        }
        set{
            self.layer.shadowRadius = newValue
        }
    }

    @IBInspectable var shadowOpacity: Float{
        get{
            return self.layer.shadowOpacity
        }
        set{
            self.layer.shadowOpacity = newValue
        }
    }
}



//MARK: - Present view over an already presented view
extension UIApplication {
    class func topViewController(viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = viewController as? UINavigationController {
            return topViewController(viewController: nav.visibleViewController)
        }
        if let tab = viewController as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(viewController: selected)
            }
        }
        if let presented = viewController?.presentedViewController {
            return topViewController(viewController: presented)
        }
        return viewController
    }
}


@IBDesignable
class DesignableLabel: UILabel {
    @IBInspectable var LineHeight: CGFloat = 20 {
        didSet {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.minimumLineHeight = LineHeight
            paragraphStyle.maximumLineHeight = LineHeight
            paragraphStyle.alignment = self.textAlignment

            let attrString = NSMutableAttributedString(string: text!)
            attrString.addAttribute(NSAttributedString.Key.font, value: font!, range: NSRange(location: 0, length: attrString.length))
            attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attrString.length))
            attributedText = attrString
        }
    }
}

// humburger menu animation
extension CATransition {

//New viewController will appear from bottom of screen.
func segueFromBottom() -> CATransition {
    self.duration = 0.375 //set the duration to whatever you'd like.
    self.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    self.type = CATransitionType.moveIn
    self.subtype = CATransitionSubtype.fromTop
    return self
}
//New viewController will appear from top of screen.
func segueFromTop() -> CATransition {
    self.duration = 0.375 //set the duration to whatever you'd like.
    self.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    self.type = CATransitionType.moveIn
    self.subtype = CATransitionSubtype.fromBottom
    return self
}
 //New viewController will appear from left side of screen.
func segueFromLeft() -> CATransition {
    self.duration = 0.1 //set the duration to whatever you'd like.
    self.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    self.type = CATransitionType.moveIn
    self.subtype = CATransitionSubtype.fromLeft
    return self
}
//New viewController will pop from right side of screen.
func popFromRight() -> CATransition {
    self.duration = 0.1 //set the duration to whatever you'd like.
    self.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    self.type = CATransitionType.reveal
    self.subtype = CATransitionSubtype.fromRight
    return self
}
//New viewController will appear from left side of screen.
func popFromLeft() -> CATransition {
    self.duration = 0.1 //set the duration to whatever you'd like.
    self.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    self.type = CATransitionType.reveal
    self.subtype = CATransitionSubtype.fromLeft
    return self
   }
}




@IBDesignable extension UIView
{

    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }

    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}


extension UIButton
{
    func activateSdWebImageLoader()
    {
        setImage(UIImage(named: PlaceholdingImages.white.rawValue), for: .normal)
        sd_imageIndicator = SDWebImageActivityIndicator.gray
    }
    
    func setPlaceholder(placeholderImage: PlaceholdingImages)
    {
        DispatchQueue.main.async { [weak self] in
            self?.setImage(UIImage(named: placeholderImage.rawValue), for: .normal)
        }
    }
    
    func addPadding(_ padding: CGFloat)
    {
        titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -padding, bottom: 0.0, right: -padding)
        contentEdgeInsets = UIEdgeInsets(top: 0.0, left: padding, bottom: 0.0, right: padding)
    }
    
}

extension UIImageView
{
    func activateSdWebImageLoader()
    {
        image = UIImage(named: PlaceholdingImages.white.rawValue)
        sd_imageIndicator = SDWebImageActivityIndicator.gray
    }
    
    func setPlaceholder(placeholderImage: PlaceholdingImages)
    {
        DispatchQueue.main.async { [weak self] in
            self?.image = UIImage(named: placeholderImage.rawValue)
        }
    }
}

enum PlaceholdingImages: String
{
    case white = "whiteBackground"
    case profile = "camera"
    case product = "productPlaceholder"
    case category = "ico_splash"
}
