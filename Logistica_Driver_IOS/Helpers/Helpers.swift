
import UIKit




func mainNavigationController() -> UINavigationController?
{
    return UIApplication.shared.windows.first { $0.isKeyWindow }?.rootViewController as? UINavigationController
    
}

func animateKeyWindow(animation: UIView.AnimationOptions, duration: TimeInterval = 0.5, completion: @escaping () -> () )
{
    DispatchQueue.main.async {
        guard
            let appDelegate = UIApplication.shared.delegate as? AppDelegate,
            let mainWindow = appDelegate.window else
        { return }
                
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
            completion()
        }
        mainWindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainWindow, duration: duration, options: animation, animations: nil, completion: nil)
    }
}

class CustomButton : UIButton  // A way to pass data through an objectiveC selector set on a UIButton
{
    var index : Int = -1
    
    convenience init(index: Int)
    {
        self.init()
        self.index = index
    }
}

func currentDateToTimeStamp () -> Int
{
    return  Int(Date().timeIntervalSince1970)
    
}



func isValidEmail(_ mail:String) -> Bool
{
    let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegex)
    return emailTest.evaluate(with: mail)
}

func isValidPhone(_ phone:String) -> Bool
{
    let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
    let inputString = phone.components(separatedBy: charcterSet)
    let filtered = inputString.joined(separator: "")
    return  phone == filtered
}
  

func dateToString(_ date: Date) -> String?
{
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd"
    formatter.locale = Locale(identifier: "en")
    return formatter.string(from: date)
}

struct PlaceItem {
    var addressId: String?
    var adressName: String?
    var adressSecName: String?
    
}

struct Vadlidator {
    var shouldICallNoteVC: Bool?
    var shouldICallPromoVC: Bool?
    var shouldICallDatePickerVC: Bool?
    var houldICallPaymentVC: Bool?
}
